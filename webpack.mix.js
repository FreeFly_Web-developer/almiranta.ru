const mix = require('laravel-mix');
// mix.js('resources/js/app.js', 'public/js')
//     .postCss('resources/css/app.css', 'public/css', [
//         //
//     ]);

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (mix.inProduction())
{
    // generic
    mix.setPublicPath('public/compressed')
        .js('resources/js/app.js', 'public/compressed/js/all.min.js')
        .sass('resources/sass/app.scss', 'public/compressed/css/styles.min.css')
        .vue()
        .version();

    // backend
    mix.setPublicPath('public/compressed')
        .js('resources/backend/js/app.js', 'public/compressed/js/backend/all.min.js')
        .sass('resources/backend/sass/app.scss', 'public/compressed/css/backend/styles.min.css')
        .vue()
        .version();
} else
{
    // generic
    mix.js('resources/js/app.js', 'public/js/all.js')
        .sass('resources/sass/app.scss', 'public/css/styles.css')
        .vue()

        // backend
        .js('resources/backend/js/app.js', 'public/js/backend/all.js')
        .sass('resources/backend/sass/app.scss', 'public/css/backend/styles.css')
            .vue()

    if (!mix.inProduction())
    {
        mix.sourceMaps(true, 'source-map') // Remove to increase performance or if source map appears in production
    }

    mix.browserSync({
        host  : process.env.APP_URL.replace('http://', ''),
        proxy : process.env.APP_URL.replace('http://', ''),
        open  : false,
        ui    : false
    }).version();
}
