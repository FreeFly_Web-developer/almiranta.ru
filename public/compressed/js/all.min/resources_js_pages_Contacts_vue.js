"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_pages_Contacts_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/Contacts.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/Contacts.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _layouts_Phones__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../layouts/Phones */ "./resources/js/layouts/Phones.vue");
/* harmony import */ var _layouts_Email__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layouts/Email */ "./resources/js/layouts/Email.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'Contacts',
  components: {
    Phones: _layouts_Phones__WEBPACK_IMPORTED_MODULE_0__["default"],
    Email: _layouts_Email__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      showMap: false
    };
  },
  computed: _objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_2__.mapState)({
    page: function page(state) {
      return state.page;
    },
    settings: function settings(state) {
      return state.settings;
    }
  })), {}, {
    phones: function phones() {
      var _this = this;

      if (Object.values(this.settings).length) {
        return this.settings.find(function (item) {
          return item.name === 'phones';
        }).json_body.map(function (item) {
          item.formatted = _this.formatPhone(item.number);
          return item;
        });
      }

      return [];
    },
    email: function email() {
      if (Object.values(this.settings).length) {
        return this.settings.find(function (item) {
          return item.name === 'email';
        }).json_body.email;
      }

      return '';
    }
  }),
  methods: {
    formatPhone: function formatPhone(phone) {
      return phone.replaceAll(' ', '').replace('-', '');
    }
  }
});

/***/ }),

/***/ "./resources/js/pages/Contacts.vue":
/*!*****************************************!*\
  !*** ./resources/js/pages/Contacts.vue ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Contacts_vue_vue_type_template_id_2d69ed48___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Contacts.vue?vue&type=template&id=2d69ed48& */ "./resources/js/pages/Contacts.vue?vue&type=template&id=2d69ed48&");
/* harmony import */ var _Contacts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Contacts.vue?vue&type=script&lang=js& */ "./resources/js/pages/Contacts.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Contacts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Contacts_vue_vue_type_template_id_2d69ed48___WEBPACK_IMPORTED_MODULE_0__.render,
  _Contacts_vue_vue_type_template_id_2d69ed48___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Contacts.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Contacts.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/pages/Contacts.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Contacts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Contacts.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/Contacts.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Contacts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Contacts.vue?vue&type=template&id=2d69ed48&":
/*!************************************************************************!*\
  !*** ./resources/js/pages/Contacts.vue?vue&type=template&id=2d69ed48& ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Contacts_vue_vue_type_template_id_2d69ed48___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Contacts_vue_vue_type_template_id_2d69ed48___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Contacts_vue_vue_type_template_id_2d69ed48___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Contacts.vue?vue&type=template&id=2d69ed48& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/Contacts.vue?vue&type=template&id=2d69ed48&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/Contacts.vue?vue&type=template&id=2d69ed48&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/Contacts.vue?vue&type=template&id=2d69ed48& ***!
  \***************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "page contacts__page page__default" }, [
    _vm.page && _vm.page.json_body
      ? _c("div", { staticClass: "d-flex flex-column" }, [
          _c(
            "div",
            {
              staticClass:
                "order-1 order-lg-0 mt-4 mt-lg-0 contacts__page-loader"
            },
            [
              _vm._m(0),
              _vm._v(" "),
              _c("div", { staticClass: "contacts__page-iframe" }, [
                _c("iframe", {
                  staticClass: "position-relative",
                  attrs: {
                    src: _vm.page.json_body.yandex,
                    width: "100%",
                    height: "600",
                    frameborder: "0"
                  }
                })
              ])
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "container order-0 order-lg-1" }, [
            _c("div", { staticClass: "mt-4 pt-lg-3" }, [
              _c("div", { staticClass: "block" }, [
                _c("img", {
                  staticClass: "mw-100 d-none d-lg-block",
                  attrs: {
                    src: "/storage/pages/map-1.png",
                    alt: _vm.page.json_body.address
                  }
                }),
                _vm._v(" "),
                _c("p", { staticClass: "mt-3 d-none d-lg-block" }, [
                  _c("i", [_vm._v(_vm._s(_vm.page.json_body.imgTitle))])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "page__content mt-lg-5" }, [
                  _c("h1", { staticClass: "font-weight-bold mb-4 pb-2" }, [
                    _vm._v(_vm._s(_vm.page.title))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "d-lg-flex align-items-lg-start" }, [
                    _c(
                      "div",
                      [
                        _c("phones", {
                          attrs: {
                            "class-name": "mb-4",
                            "class-name-phones": "mb-1",
                            phones: _vm.phones,
                            "show-text": true
                          }
                        }),
                        _vm._v(" "),
                        _c("email", {
                          attrs: { "class-name": "text-main", email: _vm.email }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass:
                          "d-flex align-items-lg-center mx-lg-5 px-lg-5 my-4 my-lg-0 text-nowrap"
                      },
                      [
                        _c(
                          "svg",
                          {
                            attrs: {
                              width: "12",
                              height: "17",
                              viewBox: "0 0 12 17",
                              fill: "none",
                              xmlns: "http://www.w3.org/2000/svg"
                            }
                          },
                          [
                            _c("path", {
                              attrs: {
                                d:
                                  "M5.84486 17C5.47378 17 5.14149 16.8076 4.95601 16.4852C4.95405 16.4818 4.95216 16.4784 4.95027 16.475L0.734733 8.75903C-0.271522 6.91722 -0.242668 4.73109 0.81193 2.91116C1.84365 1.13068 3.6762 0.042915 5.71401 0.00134473C5.80107 -0.000448242 5.88859 -0.000448242 5.97558 0.00134473C8.01342 0.042915 9.84597 1.13068 10.8777 2.91116C11.9323 4.73109 11.9612 6.91718 10.9549 8.75903L6.73939 16.475C6.73749 16.4784 6.7356 16.4818 6.73364 16.4852C6.5482 16.8075 6.21594 17 5.84486 17ZM5.84483 1.06252C5.80834 1.06252 5.77195 1.06288 5.73562 1.06361C4.07218 1.09754 2.57518 1.98735 1.73119 3.44388C0.864357 4.93984 0.840385 6.73636 1.66711 8.24956L5.84483 15.8962L10.0225 8.24959C10.8492 6.73636 10.8253 4.93984 9.9584 3.44388C9.11441 1.98739 7.61741 1.09754 5.95397 1.06361C5.91774 1.06288 5.88132 1.06252 5.84483 1.06252Z",
                                fill: "#373F42"
                              }
                            }),
                            _vm._v(" "),
                            _c("path", {
                              attrs: {
                                d:
                                  "M5.84375 7.70313C4.52555 7.70313 3.45312 6.6307 3.45312 5.3125C3.45312 3.9943 4.52555 2.92188 5.84375 2.92188C7.16195 2.92188 8.23438 3.9943 8.23438 5.3125C8.23438 6.6307 7.16198 7.70313 5.84375 7.70313ZM5.84375 3.98438C5.11142 3.98438 4.51563 4.58017 4.51563 5.3125C4.51563 6.04483 5.11142 6.64063 5.84375 6.64063C6.57608 6.64063 7.17188 6.04483 7.17188 5.3125C7.17188 4.58017 6.57608 3.98438 5.84375 3.98438Z",
                                fill: "#373F42"
                              }
                            })
                          ]
                        ),
                        _vm._v(" "),
                        _c("span", { staticClass: "ml-2" }, [
                          _vm._v(_vm._s(_vm.page.json_body.address))
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "d-flex align-items-start" }, [
                      _c("p", { staticClass: "mb-0" }, [
                        _c(
                          "svg",
                          {
                            attrs: {
                              width: "16",
                              height: "16",
                              viewBox: "0 0 16 16",
                              fill: "none",
                              xmlns: "http://www.w3.org/2000/svg"
                            }
                          },
                          [
                            _c("path", {
                              attrs: {
                                d:
                                  "M14.3164 4.20996C13.985 4.37028 13.8464 4.76904 14.0067 5.10026C14.4447 6.00505 14.6667 6.98031 14.6667 8C14.6667 11.6759 11.6759 14.6667 8 14.6667C4.32406 14.6667 1.33333 11.6759 1.33333 8C1.33333 4.32406 4.32406 1.33333 8 1.33333C9.52328 1.33333 10.9543 1.83073 12.1387 2.77165C12.4259 3.00098 12.846 2.95296 13.0754 2.66471C13.3047 2.37663 13.2567 1.95703 12.9683 1.72803C11.5661 0.613607 9.8016 0 8 0C3.58903 0 0 3.58903 0 8C0 12.411 3.58903 16 8 16C12.411 16 16 12.411 16 8C16 6.77767 15.7331 5.60628 15.2067 4.51969C15.0467 4.18766 14.6466 4.04932 14.3164 4.20996Z",
                                fill: "#373F42"
                              }
                            }),
                            _vm._v(" "),
                            _c("path", {
                              attrs: {
                                d:
                                  "M8.00016 2.66675C7.63216 2.66675 7.3335 2.96541 7.3335 3.33341V8.00008C7.3335 8.36808 7.63216 8.66675 8.00016 8.66675H11.3335C11.7015 8.66675 12.0002 8.36808 12.0002 8.00008C12.0002 7.63208 11.7015 7.33342 11.3335 7.33342H8.66683V3.33341C8.66683 2.96541 8.36816 2.66675 8.00016 2.66675Z",
                                fill: "#373F42"
                              }
                            })
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", {
                        staticClass: "ml-2 contacts__page-schedule",
                        domProps: {
                          innerHTML: _vm._s(_vm.page.json_body.schedule)
                        }
                      })
                    ])
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "container order-2 mt-3 mt-md-5" }, [
            _c(
              "a",
              {
                staticClass:
                  "contacts__page-show-map text-main d-inline-block font-weight-bold",
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    _vm.showMap = !_vm.showMap
                  }
                }
              },
              [
                _vm.showMap ? [_vm._v("Скрыть")] : [_vm._v("Показать")],
                _vm._v("\n        маршрут от метро\n      ")
              ],
              2
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "order-3 mt-2 mt-md-4 pt-2" }, [
            _vm.showMap
              ? _c("div", { staticClass: "contacts__page-map" }, [
                  _c("img", {
                    staticClass: "d-none d-md-block w-100",
                    attrs: {
                      src: "/storage/pages/map-2.png",
                      alt: _vm.page.json_body.address
                    }
                  }),
                  _vm._v(" "),
                  _c("img", {
                    staticClass: "d-block d-md-none mx-auto mw-100",
                    attrs: {
                      src: "/storage/pages/map-3.png",
                      alt: _vm.page.json_body.address
                    }
                  })
                ])
              : _vm._e()
          ])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "d-flex align-items-center justify-content-center position-absolute w-100 h-100"
      },
      [
        _c(
          "div",
          {
            staticClass: "spinner-border",
            staticStyle: { width: "3rem", height: "3rem" },
            attrs: { role: "status" }
          },
          [_c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])]
        )
      ]
    )
  }
]
render._withStripped = true



/***/ })

}]);