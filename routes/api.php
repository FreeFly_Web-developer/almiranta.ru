<?php

use App\Http\Controllers\BannerController;
use App\Http\Controllers\CoinController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\SettingsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/Coins', [CoinController::class, 'getCoinsList']);
Route::post('/Coins', [CoinController::class, 'addCoin']);
Route::get('/Coin', [CoinController::class, 'getCoinBySlug']);
Route::get('/Coin/{coinId}', [CoinController::class, 'getCoinById']);
Route::patch('/Coin/{coinId}', [CoinController::class, 'updateCoin']);
Route::delete('/Coin/{coinId}', [CoinController::class, 'deleteCoin']);

Route::get('/Countries', [CountryController::class, 'getCountriesList']);
Route::post('/Countries', [CountryController::class, 'addCountry']);
Route::get('/Country/{countryId}', [CountryController::class, 'getCountry']);
Route::patch('/Country/{countryId}', [CountryController::class, 'updateCountry']);
Route::delete('/Country/{countryId}', [CountryController::class, 'deleteCountry']);

Route::get('/News', [NewsController::class, 'getNewsList']);
Route::post('/News', [NewsController::class, 'addNewsArticle']);
Route::get('/News/article', [NewsController::class, 'getNewsArticleBySlug']);
Route::get('/News/article/{newsArticleId}', [NewsController::class, 'getNewsArticleById']);
Route::patch('/News/article/{newsArticleId}', [NewsController::class, 'updateNewsArticle']);
Route::delete('/News/article/{newsArticleId}', [NewsController::class, 'deleteNewsArticle']);

Route::get('/Pages', [PageController::class, 'getPagesList']);
Route::get('/Page', [PageController::class, 'getPageBySlug']);
Route::get('/Page/{pageId}', [PageController::class, 'getPageById']);
Route::patch('/Page/{pageId}', [PageController::class, 'updatePage']);

Route::get('/Banners', [BannerController::class, 'getBannersList']);
Route::post('/Banners', [BannerController::class, 'addBanner']);
Route::get('/Banner/{bannerId}', [BannerController::class, 'getBanner']);
Route::patch('/Banner/{bannerId}', [BannerController::class, 'updateBanner']);
Route::delete('/Banner/{bannerId}', [BannerController::class, 'deleteBanner']);

Route::get('/Settings', [SettingsController::class, 'getSettingsList']);
Route::patch('/Settings/{settingsItemId}', [SettingsController::class, 'updateSettingsItem']);
