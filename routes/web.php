<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\SettingsController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/file', [ImageController::class, 'uploadImages']);
Route::get('/get-events', [SettingsController::class, 'events']);
Route::post('/store-event', [SettingsController::class, 'storeEvent']);

// Backend routes go first, or it will be rewritten by wildcard
Route::get('/backend/login', [LoginController::class, 'show'])->name('login');
Route::post('/backend/login', [LoginController::class, 'authenticate'])->name('login-attempt');
Route::get('/backend/logout', [LoginController::class, 'logout'])->name('logout');


Route::get('/backend', [AdminController::class, 'index'])->name('admin.home');
Route::get('/backend/{any}', [AdminController::class, 'index'])->where('any', '.*');

// Openapi specification
Route::get('/openapi_spec', function () {
  return Storage::get('private/files/openapi.json');
});

// Sitemap.xml
Route::get('/sitemap.xml', function () {
    return (new SettingsController())->generateSitemap();
});

// Site pages
Route::get('/', [PageController::class, 'index']);
Route::get('/{section}/{page?}', [PageController::class, 'section'])->where([
  'section' => '[\w\-]+',
  'page'    => '[\w\-]+',
]);
