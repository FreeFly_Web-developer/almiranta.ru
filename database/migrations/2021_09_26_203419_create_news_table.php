<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 255)->comment('Идентификатор страницы для URL, сгенерированный на основе name');
            $table->string('name', 255)->comment('Название новости');
            $table->text('text')->comment('Текст статьи');
            $table->boolean('isActive')->default(false)->comment('Признак активности');
            $table->string('metaTitle')->comment('META-Title');
            $table->string('metaDescription')->nullable()->comment('META-Description');
            $table->string('metaKeywords')->nullable()->comment('META-Keywords');
            $table->dateTime('releasedAt')->comment('Дата публикации');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
