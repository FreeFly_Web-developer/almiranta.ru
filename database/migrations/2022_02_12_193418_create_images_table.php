<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('imageable_id')->comment('Id of the model');
            $table->string('imageable_type')->comment('Name of the model');
            $table->string('name')->comment('Name of the image property');
            $table->string('value')->nullable()->comment('Value of the image property');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
