<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 255)->comment('Идентификатор страницы для URL, сгенерированный на основе name');
            $table->string('name', 255)->comment('Название монеты');
            $table->text('description')->nullable()->comment('Развёрнутое описание монеты');
            $table->float('weight')->unsigned()->comment('Общая масса монеты');
            $table->float('weightPure')->unsigned()->comment('Масса чистого металла');
            $table->tinyInteger('metalId')->unsigned()->default(1)->comment('ID металла');
            $table->integer('purity')->unsigned()->comment('Проба');
            $table->float('diameter')->unsigned()->nullable()->comment('Диаметр монеты');
            $table->float('thickness')->unsigned()->nullable()->comment('Ширина гурта (толщина) монеты');
            $table->integer('year')->unsigned()->nullable()->comment('Год выпуска');
            $table->integer('periodFrom')->unsigned()->nullable()->comment('Начало периода выпуска');
            $table->integer('periodTo')->unsigned()->nullable()->comment('Конец периода выпуска');
            $table->integer('mintage')->unsigned()->nullable()->comment('Тираж');
            $table->bigInteger('countryId')->unsigned()->default(0)->comment('ID государства-эмитента');
            $table->string('denomination')->comment('Номинал');
            $table->string('denominationCurrency')->comment('Валюта номинала');
            $table->string('mint')->nullable()->comment('Монетный двор');
            $table->boolean('isActive')->default(false)->comment('Признак активности');
            $table->tinyInteger('conditionId')->unsigned()->default(5)->comment('ID состояния монеты');
            $table->double('priceBuy')->unsigned()->nullable()->comment('Цена покупки');
            $table->double('priceSell')->unsigned()->comment('Цена продажи');
            $table->double('nofMinDiscussPrice')->unsigned()->nullable()
                ->comment('Минимальная сумма на покупку свыше которой, цена обсуждается отдельно');
            $table->integer('nofAvailable')->unsigned()->nullable()
                ->comment('Максимальное доступное количество монет данного типа');
            $table->integer('order')->unsigned()->default(0)->comment('Порядок вывода монеты в списке');
            $table->string('metaTitle')->comment('META-Title');
            $table->string('metaDescription')->nullable()->comment('META-Description');
            $table->string('metaKeywords')->nullable()->comment('META-Keywords');
            $table->string('altObverse')->nullable()->comment('Описание изображения аверса');
            $table->string('altReverse')->nullable()->comment('Описание изображения реверса');
            $table->timestamps();

            $table->index('slug');
            $table->unique(['slug', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins');
    }
}
