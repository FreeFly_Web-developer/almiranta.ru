<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Название страны');
            $table->boolean('isActive')->default(false)->comment('Признак активности');
            $table->timestamps();

            $table->unique(['name']);
        });

        Schema::table('coins', function (Blueprint $table) {
            $table->foreign('countryId')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coins', function ($table) {
          $table->dropForeign('coins_countryId_foreign');
        });

        Schema::dropIfExists('countries');
    }
}
