<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 255)->comment('Идентификатор страницы для URL, сгенерированный на основе name');
            $table->string('title', 255)->comment('Название страницы');
            $table->json('json_body')->comment('Тело страницы и прочие настройки в виде JSON строки');
            $table->string('metaTitle')->comment('META-Title');
            $table->string('metaDescription')->nullable()->comment('META-Description');
            $table->string('metaKeywords')->nullable()->comment('META-Keywords');
            $table->timestamps();

            $table->index('slug');
            $table->unique(['slug', 'title']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
