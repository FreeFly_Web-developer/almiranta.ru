<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('Текст баннера');
            $table->string('text')->nullable()->comment('Полнотекстовое описание баннера');
            $table->string('alt')->nullable()->comment('Описание изображения/ссылки баннера');
            $table->string('url')->nullable()->comment('URL страницы, на которую ведёт ссылка баннера');
            $table->boolean('isActive')->default(false)->comment('Признак активности');
            $table->integer('order')->unsigned()->default(0)->comment('Порядок вывода баннера');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
