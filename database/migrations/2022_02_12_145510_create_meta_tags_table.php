<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetaTagsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('meta_tags', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('metatagable_id')->comment('Id of the model');
            $table->string('metatagable_type')->comment('Name of the model');
            $table->string('name')->comment('Name of the tag');
            $table->string('value')->nullable()->comment('Value of the tag');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_tags');
    }
}
