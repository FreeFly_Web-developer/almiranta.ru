<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CountrySeeder::class,
            CoinSeeder::class,
            NewsSeeder::class,
            PageSeeder::class,
            BannerSeeder::class,
            UserSeeder::class,
            SettingsSeeder::class,
        ]);
    }
}
