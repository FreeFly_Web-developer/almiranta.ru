<?php

namespace Database\Seeders;

use App\Models\Banner;
use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banners = [
          [
            'title'     => 'Золотые инвестиционные монеты на индивидуальных условиях',
            'isActive'  => true,
            'order'     => 0,
          ],
//          [
//            'title'     => 'Золотые 1 инвестиционные монеты на индивидуальных условиях 1',
//            'isActive'  => false,
//            'order'     => 1,
//          ],
//          [
//            'title'     => 'Золотые 2 инвестиционные монеты на индивидуальных условиях 2',
//            'isActive'  => true,
//            'order'     => 2,
//          ]
        ];

        foreach ($banners as $banner) {
            $banner = new Banner($banner);
            $banner->save();
        }
    }
}
