<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name'              => 'Администратор',
                'email'             => 'info@almiranta.ru',
                'password'          => bcrypt('Zoloto999*'),
                'remember_token'    => 60 * 60 * 24 * 30
            ],

        ];

        foreach ($users as $user) {
            $user = new User($user);
            $user->save();
        }
    }
}
