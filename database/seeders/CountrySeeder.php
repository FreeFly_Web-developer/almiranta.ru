<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = array(
          array('id' => '1','name' => 'Не определено','isActive' => '1','created_at' => '2021-10-13 01:35:16','updated_at' => '2021-10-13 01:35:16'),
          array('id' => '2','name' => 'Россия','isActive' => '1','created_at' => '2021-10-13 01:35:16','updated_at' => '2021-10-13 01:35:16'),
          array('id' => '3','name' => 'Великобритания','isActive' => '1','created_at' => '2021-10-13 01:35:16','updated_at' => '2021-10-13 01:35:16'),
          array('id' => '7','name' => 'Австралия','isActive' => '1','created_at' => '2021-10-13 13:27:45','updated_at' => '2021-10-13 13:29:46'),
          array('id' => '8','name' => 'Австрия','isActive' => '1','created_at' => '2021-10-13 13:27:53','updated_at' => '2021-10-13 13:29:42'),
          array('id' => '9','name' => 'Канада','isActive' => '1','created_at' => '2021-10-13 13:28:07','updated_at' => '2021-10-13 13:29:59'),
          array('id' => '10','name' => 'США','isActive' => '1','created_at' => '2021-10-13 13:28:17','updated_at' => '2021-10-13 13:29:54'),
          array('id' => '11','name' => 'ЮАР','isActive' => '1','created_at' => '2021-10-13 13:28:32','updated_at' => '2021-10-13 13:30:06'),
          array('id' => '12','name' => 'Китай','isActive' => '1','created_at' => '2021-10-13 13:28:44','updated_at' => '2021-10-13 13:30:02')
        );

        foreach ($countries as $country) {
            $country = new Country($country);
            $country->save();
        }
    }
}
