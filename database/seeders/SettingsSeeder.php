<?php

namespace Database\Seeders;

use App\Models\Settings;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
          [
            'name'      => 'phones',
            'json_body' => [
              [
                'number' => '+7 901 339 35-35',
                'alt'   => 'Основной телефон',
              ],
              [
                'number' => '+7 968 550 44-22',
                'alt'   => 'Дополнительный телефон',
              ],
            ],
          ],
          [
            'name'      => 'email',
            'json_body' => [
              'email' => 'info@almiranta.ru',
            ],
          ],
          [
            'name'      => 'telegram',
            'json_body' => [
              [
                'account' => '+79013393535',
                'alt'   => 'Первый Telegram аккаунт',
              ],
              [
                'account' => 'Almirantagold',
                'alt'   => 'Второй Telegram аккаунт',
              ],
            ],
          ],
          [
            'name'      => 'rates',
            'json_body' => [
              'script' => '{
  "symbols": [
    {
      "proName": "FOREXCOM:SPXUSD",
      "title": "S&P 500"
    },
    {
      "description": "RTSI",
      "proName": "MOEX:RTSI"
    },
    {
      "description": "MOEX",
      "proName": "MOEX:IMOEX"
    },
    {
      "description": "USD/RUB",
      "proName": "SAXO:USDRUB"
    },
    {
      "description": "EUR/RUB",
      "proName": "SAXO:EURRUB"
    },
    {
      "description": "EUR/USD",
      "proName": "SAXO:EURUSD"
    },
    {
      "description": "BTC/USD",
      "proName": "CURRENCYCOM:BTCUSD"
    },
    {
      "description": "GOLD",
      "proName": "CURRENCYCOM:GOLD"
    }
  ],
  "showSymbolLogo": false,
  "colorTheme": "dark",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "ru"
}
',
            ],
          ],
        ];

        foreach ($settings as $option) {
            $option = new Settings($option);
            $option->save();
        }
    }
}
