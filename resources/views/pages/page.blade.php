@extends('layouts.index')

@section('meta')
    @component('layouts.meta')
        @slot('page', $subPage ?? $page)
    @endcomponent
@endsection

@section('content')
    @if(!$subPage)
        @includeIf('pages.components.' . $page->id)
    @else
        @includeIf('pages.components.' . $subPage->type)
    @endif
@endsection