<div class="page news__page page__default">
    <div class="container">
        <breadcrumbs :links="{{ json_encode($breadcrumbs, true) }}"></breadcrumbs>
        <div class="page__content">
            <h1 class="font-weight-bold mb-3 mt-4 mt-md-0">{{ $page->json_body['title'] }}</h1>
        </div>
        <div class="news row">
            @foreach($articles['news'] as $article)
                <div class="col-12 col-md-4 article__wrap mb-4">
                    <news-article :article="{{ json_encode($article, true) }}"></news-article>
                </div>
            @endforeach
        </div>
        <pagination
{{--                @change-page="changePage"--}}
                :page="{{ json_encode($articles['page'], true) }}"
                :limit="{{ json_encode($articles['limit'], true) }}"
                :count="{{ json_encode(count($articles), true) }}"
                :total="{{ json_encode($articles['nofTotal'], true) }}"
        ></pagination>
    </div>
</div>