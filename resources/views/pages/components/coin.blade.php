<div class="page coin__page page__default">
    <div class="container">
        <breadcrumbs :links="{{ json_encode($breadcrumbs, true) }}"></breadcrumbs>
        <div class="coin__content">
            <div class="row">
                <div class="col-12 col-lg-3">
                    <h1 class="font-weight-bold coin__content-name mt-3 mb-3 d-lg-none">{{ $subPage['name'] }}</h1>
                    <div class="coin__content-image text-center pt-4 pb-4 d-flex d-lg-block justify-content-around">
                        @foreach($subPage->images as $image)
                            @if($image !== '//via.placeholder.com/232x232')
                                <div class="{{ $loop->index === 0 ? 'mb-lg-4' : '' }}">
                                    <img src="{{ $image }}" alt="{{ $subPage->image_alt[$loop->iteration] }}">
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="col-12 col-lg-9">
                    <div class="coin__content-info mt-3">
                        <h1 class="font-weight-bold coin__content-name d-none d-lg-block">{{ $subPage['name'] }}</h1>
                        <div class="d-md-flex align-items-start border-bottom pb-4 mt-3">
                            <ul class="list-unstyled coin__parameters mb-md-0 mr-md-4 pr-md-2">
                                @if($subPage->priceBuy)
                                    <li class="d-flex align-items-center mb-1">
                                        <div class="coin__parameters-name coin__parameters-name--solid">{{ $subPage['priceSell'] ? 'Покупка' : ' Только покупка' }}</div>
                                        <div class="coin__parameters-value text-brand-color font-weight-bold">{{ number_format($subPage['priceBuy'], 0, ',', ' ') }}&nbsp;₽</div>
                                    </li>
                                @endif
                                @if($subPage->priceSell)
                                    <li class="d-flex align-items-center">
                                        <div class="coin__parameters-name coin__parameters-name--solid">{{ $subPage['priceBuy'] ? 'Продажа' : ' Только продажа' }}</div>
                                        <div class="coin__parameters-value text-brand-color font-weight-bold">{{ number_format($subPage['priceSell'], 0, ',' ,' ') }}&nbsp;₽</div>
                                    </li>
                                @endif
                            </ul>
                            <div class="a-tooltip position-relative mt-n1">При объёме операции свыше<br>600 000₽ цена обсуждается</div>
                        </div>
                        <ul class="list-unstyled coin__parameters border-bottom pb-3 mt-3">
                            @foreach($parameters as $parameter)
                                <li class="d-flex align-items-center mb-1 coin__parameters--{{ $parameter['name'] }}">
                                    <div class="coin__parameters-name">{{ $parameter['title'] }}</div>
                                    <div class="coin__parameters-value">
                                        @if(!in_array($parameter['name'], ['metal', 'condition']))
                                            {{ $subPage->toArray()[$parameter['name']] }}
                                        @else
                                            {{ $subPage->toArray()[$parameter['name']]['name'] }}
                                        @endif
                                        @if(isset($parameter['postfix']))
                                            {{ $parameter['postfix'] }}
                                        @endif
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        @if($subPage->description)
                            <p class="coin__description">{!! $subPage['description'] !!}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>