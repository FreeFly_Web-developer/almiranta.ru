<div class="page contacts__page page__default">
    <div class="d-flex flex-column">
        <div class="order-1 order-lg-0 mt-4 mt-lg-0 contacts__page-loader">
            <div class="d-flex align-items-center justify-content-center position-absolute w-100 h-100">
                <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
            <div class="contacts__page-iframe">
                <iframe class="position-relative" src="{!! $page->json_body['yandex'] !!}" width="100%" frameborder="0"></iframe>
            </div>
        </div>
        <div class="container order-0 order-lg-1">
            <div class="mt-4 pt-lg-3">
                <div class="block">
                    <img class="mw-100 d-none d-lg-block" src="/storage/pages/map-1.png" alt="{{ $page->json_body['address'] }}">
                    <p class="mt-3 d-none d-lg-block"><i>{{ $page->json_body['imgTitle'] }}</i></p>
                    <div class="page__content mt-lg-5">
                        <h1 class="font-weight-bold mb-4 pb-2">{{ $page->title }}</h1>
                        <div class="d-lg-flex align-items-lg-start">
                            <div>
                                <phones class-name="mb-4" class-name-phones="mb-1" :phones="{{ json_encode($phones, true) }}" :show-text="true"></phones>
                                <email class-name="text-main" :email="{{ json_encode($email, true) }}"></email>
                            </div>
                            <div class="d-flex align-items-lg-center mx-lg-5 px-lg-5 my-4 my-lg-0 text-nowrap">
                                <svg width="12" height="17" viewBox="0 0 12 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5.84486 17C5.47378 17 5.14149 16.8076 4.95601 16.4852C4.95405 16.4818 4.95216 16.4784 4.95027 16.475L0.734733 8.75903C-0.271522 6.91722 -0.242668 4.73109 0.81193 2.91116C1.84365 1.13068 3.6762 0.042915 5.71401 0.00134473C5.80107 -0.000448242 5.88859 -0.000448242 5.97558 0.00134473C8.01342 0.042915 9.84597 1.13068 10.8777 2.91116C11.9323 4.73109 11.9612 6.91718 10.9549 8.75903L6.73939 16.475C6.73749 16.4784 6.7356 16.4818 6.73364 16.4852C6.5482 16.8075 6.21594 17 5.84486 17ZM5.84483 1.06252C5.80834 1.06252 5.77195 1.06288 5.73562 1.06361C4.07218 1.09754 2.57518 1.98735 1.73119 3.44388C0.864357 4.93984 0.840385 6.73636 1.66711 8.24956L5.84483 15.8962L10.0225 8.24959C10.8492 6.73636 10.8253 4.93984 9.9584 3.44388C9.11441 1.98739 7.61741 1.09754 5.95397 1.06361C5.91774 1.06288 5.88132 1.06252 5.84483 1.06252Z" fill="#373F42"/>
                                    <path d="M5.84375 7.70313C4.52555 7.70313 3.45312 6.6307 3.45312 5.3125C3.45312 3.9943 4.52555 2.92188 5.84375 2.92188C7.16195 2.92188 8.23438 3.9943 8.23438 5.3125C8.23438 6.6307 7.16198 7.70313 5.84375 7.70313ZM5.84375 3.98438C5.11142 3.98438 4.51563 4.58017 4.51563 5.3125C4.51563 6.04483 5.11142 6.64063 5.84375 6.64063C6.57608 6.64063 7.17188 6.04483 7.17188 5.3125C7.17188 4.58017 6.57608 3.98438 5.84375 3.98438Z" fill="#373F42"/>
                                </svg>
                                <span class="ml-2">{{ $page->json_body['address'] }}</span>
                            </div>
                            <div class="d-flex align-items-start">
                                <p class="mb-0">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M14.3164 4.20996C13.985 4.37028 13.8464 4.76904 14.0067 5.10026C14.4447 6.00505 14.6667 6.98031 14.6667 8C14.6667 11.6759 11.6759 14.6667 8 14.6667C4.32406 14.6667 1.33333 11.6759 1.33333 8C1.33333 4.32406 4.32406 1.33333 8 1.33333C9.52328 1.33333 10.9543 1.83073 12.1387 2.77165C12.4259 3.00098 12.846 2.95296 13.0754 2.66471C13.3047 2.37663 13.2567 1.95703 12.9683 1.72803C11.5661 0.613607 9.8016 0 8 0C3.58903 0 0 3.58903 0 8C0 12.411 3.58903 16 8 16C12.411 16 16 12.411 16 8C16 6.77767 15.7331 5.60628 15.2067 4.51969C15.0467 4.18766 14.6466 4.04932 14.3164 4.20996Z" fill="#373F42"/>
                                        <path d="M8.00016 2.66675C7.63216 2.66675 7.3335 2.96541 7.3335 3.33341V8.00008C7.3335 8.36808 7.63216 8.66675 8.00016 8.66675H11.3335C11.7015 8.66675 12.0002 8.36808 12.0002 8.00008C12.0002 7.63208 11.7015 7.33342 11.3335 7.33342H8.66683V3.33341C8.66683 2.96541 8.36816 2.66675 8.00016 2.66675Z" fill="#373F42"/>
                                    </svg>
                                </p>
                                <div class="ml-2 contacts__page-schedule">
                                    {!! $page->json_body['schedule'] !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container order-2 mt-3 mt-md-5">
            <a href="#" class="contacts__page-show-map text-main d-inline-block font-weight-bold">Показать маршрут от метро</a>
        </div>
        <div class="order-3 mt-2 mt-md-4 pt-2">
            <div class="contacts__page-map d-none">
                <img class="d-none d-md-block w-100" src="/storage/pages/map-2.png" alt="{{ $page->json_body['address'] }}">
                <img class="d-block d-md-none mx-auto mw-100" src="/storage/pages/map-3.png" alt="{{ $page->json_body['address'] }}">
            </div>
        </div>
    </div>
</div>