<div class="page news__page page__default">
    <div class="container pb-5">
        <breadcrumbs :links="{{ json_encode($breadcrumbs, true) }}"></breadcrumbs>
        <div class="page__content mx-auto">
            <h1 class="font-weight-bold mb-2 mt-4 mt-md-0">{{ $subPage->name }}</h1>
            <p class="text-gray news__page-date">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $subPage->releasedAt)->format('d.m.Y H:i:s') }}</p>
            <div class="mb-3"><img class="w-100" src="{{ $subPage->image }}" alt="{{ $subPage->image_alt }}"></div>
            <div class="news__page-description">{!! $subPage->text !!}</div>
        </div>
    </div>
</div>