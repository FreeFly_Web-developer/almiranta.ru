<div class="page why-us__page page__default">
    <div class="container">
        <breadcrumbs :links="{{ json_encode($breadcrumbs, true) }}"></breadcrumbs>
        <div class="page__content">
            <div class="d-lg-flex">
                <div>
                    <h1 class="font-weight-bold mb-3 mt-4 mt-md-0">{{ $page->json_body['title'] }}</h1>
                    <div>{!! $page->json_body['text'] !!}</div>
                </div>
                <div class="ml-5 pl-4 d-none d-lg-block"><img src="/storage/pages/why-us.png" alt="{{ $page->image_alt }}"></div>
            </div>
        </div>
    </div>
</div>