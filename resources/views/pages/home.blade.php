@extends('layouts.index')

@section('meta')
    @component('layouts.meta')
        @slot('page', $pages[0]->load('meta_tags'))
    @endcomponent
@endsection

@section('content')
    <div class="page home__page">
        <banners :banners="{{ json_encode($banners, true) }}"></banners>
        <div class="container">
            <div class="coins row">
                @foreach($coins['coins'] as $coin)
                    <div class="col-6 col-lg-4 col-xl-3 coin__wrap align-items-stretch">
                        <coin :coin="{{ json_encode($coin, true) }}"></coin>
                    </div>
                @endforeach
            </div>
            <div class="news row mt-5">
                <div class="col-12 d-flex align-items-end justify-content-between news__header mb-4">
                    <h3 class="mb-0">{{ $pages[2]->json_body['title'] }}</h3>
                    <a class="text-brand-color font-weight-bold" href="{{ $pages[2]->slug }}">Все новости</a>
                </div>
                @foreach($articles['news'] as $article)
                    <div class="col-12 col-md-4 article__wrap mb-4 mb-lg-0">
                        <news-article :article="{{ $article }}"></news-article>
                    </div>
                @endforeach
            </div>
            <div class="position-relative">
                <div id="questions" class="questions-label position-absolute"></div>
            </div>
            <faq
                    :title="{{ json_encode($pages[3]->json_body['title'], true) }}"
                    :questions="{{ json_encode($pages[3]->json_body['questions'], true) }}"
            ></faq>
        </div>
    </div>
@endsection