<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Almiranta Backend</title>
    <link rel="icon" type="image/png" href="/favicon.png" />
    <link rel="apple-touch-icon" href="/favicon.png" />
    @if (env('APP_ENV') === 'production')
        <link rel="stylesheet" href="{{ mix('/css/backend/styles.min.css', 'compressed') }}">
    @else
        <link rel="stylesheet" href="{{ mix('/css/backend/styles.css') }}">
    @endif
</head>
<body class="login-page">
<div class="login-box">
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="/" class="h1"><b>Almira</b>nta</a>
        </div>
        <div class="card-body">
            <p class="login-box-msg">Войдите, чтобы начать пользоваться</p>
            <form method="post">
                {{ csrf_field()  }}
                <div class="input-group mb-3">
                    <input name="email" type="email" class="form-control{{ ($errors->has('email') ? ' form-control is-invalid' : '') }}" placeholder="Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    @if ($errors->has('email'))
                    <span class="error invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="input-group mb-3">
                    <input name="password" type="password" class="form-control{{ ($errors->has('password') ? ' form-control is-invalid' : '') }}" placeholder="Пароль">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @if ($errors->has('password'))
                        <span class="error invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="row align-items-center">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember" name="remember" class="mr-1">
                            <label for="remember">
                                Запомнить
                            </label>
                        </div>
                    </div>
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Войти</button>
                    </div>
                </div>
            </form>
            <p class="mb-1" style="display: none;">
                <a href="forgot-password.html">Забыли пароль?</a>
            </p>
        </div>
    </div>
</div>
@if (env('APP_ENV') === 'production')
    <script src="{{ mix('/js/backend/all.min.js', 'compressed') }}"></script>
@else
    <script src="{{ mix('/js/backend/all.js') }}"></script>
@endif
</body>
</html>