<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Almiranta Backend</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="/favicon.png" />
    <link rel="apple-touch-icon" href="/favicon.png" />
    @if (env('APP_ENV') === 'production')
        <link rel="stylesheet" href="{{ mix('/css/backend/styles.min.css', 'compressed') }}">
    @else
        <link rel="stylesheet" href="{{ mix('/css/backend/styles.css') }}">
    @endif
</head>
<body class="hold-transition sidebar-mini">
<div id="app"></div>
@if (env('APP_ENV') === 'production')
    <script src="{{ mix('/js/backend/all.min.js', 'compressed') }}"></script>
@else
    <script src="{{ mix('/js/backend/all.js') }}"></script>
@endif
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
</body>
</html>
