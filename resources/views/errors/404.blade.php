@extends('layouts.index')

@section('meta')
    @component('layouts.meta')
        @slot('page', $page404)
    @endcomponent
@endsection

@section('content')
    <div class="page why-us__page page__default">
        <div class="container pt-5">
            {!! $page404->json_body['text'] !!}
        </div>
    </div>
@endsection