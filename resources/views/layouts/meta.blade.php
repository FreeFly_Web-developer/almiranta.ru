<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>{{ $page->metaTitle ?? config('app.name') }}</title>
<meta name="description" content="{{ $page->metaDescription }}">

<meta property="og:locale" content="ru_RU">
<meta property="og:site_name" content="{{ config('app.name') }}">
<meta property="og:type" content="article">
<meta property="og:url" content="{{ request()->fullUrl() }}">

@if($page->ogTitle)
    <meta property="og:title" content="{{ $page->ogTitle }}">
@endif
@if($page->ogDescription)
    <meta property="og:description" content="{{ $page->ogDescription }}">
@endif

@if($page->ogImage)
    <meta property="og:image" content="{{ url('/') }}{{ $page->ogImage }}">

    @if($page->ogImageWidth)
        <meta property="og:image:width" content="{{ $page->ogImageWidth }}">
    @endif

    @if($page->ogImageHeight)
        <meta property="og:image:height" content="{{ $page->ogImageHeight }}">
    @endif
@endif

<link rel="canonical" href="{{ request()->fullUrl() }}">

<meta name="robots" content="index, follow">

<link rel="icon" type="image/png" href="/favicon.png">
<link rel="apple-touch-icon" href="/favicon.png">

<meta name="yandex-verification" content="5778e85ea8f7d9cd">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MDMJBHV');</script>
<!-- End Google Tag Manager -->