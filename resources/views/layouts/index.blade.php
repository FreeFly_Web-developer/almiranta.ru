<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @yield('meta')
    @if (env('APP_ENV') === 'production')
        <link rel="stylesheet" href="{{ mix('/css/styles.min.css', 'compressed') }}">
    @else
        <link rel="stylesheet" href="{{ mix('/css/styles.css') }}">
    @endif
    <link rel="stylesheet" href="/common.css">
</head>
<body class="antialiased">
<div id="app">
    @include('layouts.header')
    @yield('content')
    <footer-main :pages="{{ $pages }}" :phones="{{ json_encode($phones, true) }}" :email="{{ json_encode($email, true) }}"></footer-main>
</div>
@if (env('APP_ENV') === 'production')
    <script src="{{ mix('/js/all.min.js', 'compressed') }}"></script>
@else
    <script src="{{ mix('/js/all.js') }}"></script>
@endif
<script>
    var mapButton = document.querySelector('.contacts__page-show-map');
    if(mapButton) {
        mapButton.addEventListener('click', function(e) {
            e.preventDefault();
            if (e.target.innerText !== 'Скрыть маршрут от метро') {
                e.target.innerText = 'Скрыть маршрут от метро';
                $('.contacts__page-map').removeClass('d-none');
            } else {
                e.target.innerText = 'Показать маршрут от метро';
                $('.contacts__page-map').addClass('d-none');
            }
        });
    }
</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MDMJBHV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>
