import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './pages/Home.vue';
import Coins from './pages/Coins.vue';
import CoinSave from './pages/CoinSave.vue';
import Countries from './pages/Countries.vue';
import News from './pages/News.vue';
import NewsSave from './pages/NewsSave.vue';
import Questions from './pages/Questions.vue';
import WhyUs from './pages/WhyUs.vue';
import Contacts from './pages/Contacts.vue';
import NewsEdit from './pages/NewsEdit.vue';
import HomeEdit from './pages/HomeEdit.vue';
import Banners from './pages/Banners.vue';
import Settings from './pages/Settings.vue';
import Page404Edit from './pages/Page404Edit.vue';
import Styles from './pages/Styles.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/backend',
            name: 'home',
            component: Home,
        },
        {
            path: '/backend/coins',
            name: 'coins',
            component: Coins,
        },
        {
            path: '/backend/coins/:id/edit',
            name: 'coin-edit',
            component: CoinSave,
        },
        {
            path: '/backend/coins/add',
            name: 'coin-add',
            component: CoinSave,
        },
        {
            path: '/backend/countries',
            name: 'countries',
            component: Countries,
        },
        {
            path: '/backend/news',
            name: 'news',
            component: News,
        },
        {
            path: '/backend/news/:id/edit',
            name: 'news-edit',
            component: NewsSave,
        },
        {
            path: '/backend/news/add',
            name: 'news-add',
            component: NewsSave,
        },
        {
            path: '/backend/questions/edit',
            name: 'questions-edit',
            component: Questions,
        },
        {
            path: '/backend/why-us/edit',
            name: 'why-us-edit',
            component: WhyUs,
        },
        {
            path: '/backend/contacts/edit',
            name: 'contacts-edit',
            component: Contacts,
        },
        {
            path: '/backend/news/edit',
            name: 'news-page-edit',
            component: NewsEdit,
        },
        {
            path: '/backend/home/edit',
            name: 'home-edit',
            component: HomeEdit,
        },
        {
            path: '/backend/404/edit',
            name: 'page-404-edit',
            component: Page404Edit,
        },
        {
            path: '/backend/banners',
            name: 'banners',
            component: Banners,
        },
        {
            path: '/backend/settings',
            name: 'settings',
            component: Settings,
        },
        {
            path: '/backend/styles',
            name: 'styles',
            component: Styles,
        },
    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    },
});

export default router;
