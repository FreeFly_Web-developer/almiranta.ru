/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

import router from './router';
import specification from '../../js/swagger';
import App from './layouts/App.vue';

// Vuex storage
import Vuex from 'vuex'
Vue.use(Vuex);

// plugins
import vSelect from 'vue-select';
Vue.component('v-select', vSelect);

import SaveButton from './components/SaveButton';
Vue.component('SaveButton', SaveButton);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const store = new Vuex.Store({
    state: {
        properties: {},
        loading: true,
    },
    mutations: {
        setLoading(state, loading) {
            state.loading = loading;
        },
        setProperties(state, data) {
            state.properties = data;
        },
    },
    actions: {
        getProperties({ commit }, schema) {
            return specification.then((client) => {
                commit('setProperties', client.spec.components.schemas[schema].properties);
            }).finally(() => {
                commit('setLoading', false);
            });
        },
        getModels({ commit }, { tag, method, data }) {
            commit('setLoading', true);
            return specification.then((client) => client.apis[tag][method](data)).
                then((res) => res).finally(() => {
                    commit('setLoading', false);
                });
        },
        getModel({ commit }, { tag, method, id }) {
            commit('setLoading', true);
            return specification.then((client) => client.apis[tag][method](id)).
                then((res) => res).finally(() => {
                    commit('setLoading', false);
                });
        },
        addModel({ commit }, { tag, method, data }) {
            return specification.then((client) => client.apis[tag][method]({}, { requestBody: data }))
                .then((res) => res);
        },
        updateModel({ commit }, { tag, method, id, data }) {
            return specification.then((client) => client.apis[tag][method](id, { requestBody: data }))
                .then((res) => res);
        },
        deleteModel({ commit }, { tag, method, id }) {
            return specification.then((client) => client.apis[tag][method](id)).then((res) => res);
        },
    },
});

const app = new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App),
});
