/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

// import router from './router';
// import specification from './swagger';
// import App from './layouts/App.vue';

// Vuex storage
// import Vuex, {mapActions, mapState} from 'vuex'
// Vue.use(Vuex);

Vue.component('rates-widget', require('./components/RatesWidget.vue').default);
Vue.component('logo', require('./layouts/Logo.vue').default);
Vue.component('email', require('./layouts/Email.vue').default);
Vue.component('phones', require('./layouts/Phones.vue').default);
Vue.component('banners', require('./components/Banners.vue').default);
Vue.component('coin', require('./components/Coin.vue').default);
Vue.component('news-article', require('./components/NewsArticle.vue').default);
Vue.component('faq', require('./components/Faq.vue').default);
Vue.component('footer-main', require('./layouts/FooterMain.vue').default);
Vue.component('breadcrumbs', require('./layouts/Breadcrumbs.vue').default);
Vue.component('pagination', require('./components/Pagination.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const store = new Vuex.Store({
//     state: {
//         page: null,
//         breadcrumbs: [
//             {
//                 path: '/',
//                 name: 'home',
//                 title: 'Главная',
//             },
//         ],
//         pages: [],
//         metaTitle: '',
//         metaDescription: '',
//         settings: [],
//         loading: true,
//     },
//     mutations: {
//         setLoading(state, loading) {
//             state.loading = loading;
//         },
//         setPage(state, data) {
//             state.page = data;
//         },
//         setBreadcrumbs(state, data) {
//             state.breadcrumbs.push(data);
//         },
//         setPages(state, data) {
//             state.pages = data;
//         },
//         setMeta(state, data) {
//             state.metaTitle = data.metaTitle;
//             state.metaDescription = data.metaDescription;
//         },
//         setSettings(state, data) {
//             state.settings = data;
//         },
//     },
//     actions: {
//         getPages({ commit }) {
//             commit('setLoading', true);
//             return specification.then((client) => client.apis.Pages.getPagesList()).
//             then((res) => {
//                 commit('setPages', res.body);
//                 const page = res.body.find((page) => page.slug === window.location.pathname);
//                 if (page) {
//                     commit('setPage', page);
//                     commit('setBreadcrumbs', {
//                         title: page.title,
//                     });
//                     commit('setMeta', {
//                         metaTitle: page.metaTitle,
//                         metaDescription: page.metaDescription,
//                     });
//                 } /*else if (!page) {
//                     const pathnames = window.location.pathname.split('/');
//                     if (!res.body.find((page) => page.id !== 1
//                         && pathnames.includes(page.slug.replace('/', '')))) {
//                         window.location.href = '/';
//                     }
//                 }*/
//             }).finally(() => {
//                 commit('setLoading', false);
//             });
//         },
//         getSettings({ commit }) {
//             commit('setLoading', true);
//             return specification.then((client) => client.apis.Settings.getSettingsList()).
//             then((res) => {
//                 commit('setSettings', res.body);
//             }).finally(() => {
//                 commit('setLoading', false);
//             });
//         },
//         getModels({ commit }, { tag, method, data }) {
//             commit('setLoading', true);
//             return specification.then((client) => client.apis[tag][method](data)).
//             then((res) => res).finally(() => {
//                 commit('setLoading', false);
//             });
//         },
//         getModel({ commit }, { tag, method, id }) {
//             commit('setLoading', true);
//             return specification.then((client) => client.apis[tag][method](id)).
//             then((res) => res).catch((errors) => {
//                 if (!errors.response.ok && errors.status === 404) {
//                     window.location.href = '/';
//                 }
//             }).finally(() => {
//                 commit('setLoading', false);
//             });
//         },
//     },
// });

const app = new Vue({
    // router,
    // store,
    el: '#app',
    // render: h => h(App),
    // metaInfo () {
    //     return {
    //         title: this.metaTitle,
    //         titleTemplate: '%s | Almiranta.ru',
    //         meta: [
    //             {
    //                 vmid: 'description',
    //                 property: 'description',
    //                 content: this.metaDescription,
    //             },
    //             {
    //                 vmid: 'og:title',
    //                 property: 'og:title',
    //                 content: this.metaTitle,
    //                 template: '%s - Almiranta.ru',
    //             },
    //             {
    //                 vmid: 'og:description',
    //                 property: 'og:description',
    //                 content: this.metaDescription,
    //             },
    //         ],
    //     };
    // },
    // computed: {
    //     ...mapState({
    //         metaTitle: (state) => state.metaTitle,
    //         metaDescription: (state) => state.metaDescription,
    //     }),
    // },
    // mounted() {
    //     this.getPages();
    //     this.getSettings();
    // },
    // methods: {
    //     ...mapActions([
    //         'getPages',
    //         'getSettings',
    //     ]),
    // },
});
