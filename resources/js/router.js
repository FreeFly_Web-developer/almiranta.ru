import Vue from 'vue';
import VueRouter from 'vue-router';
import VueMeta from 'vue-meta';

import Home from './pages/Home.vue';
import Coin from './pages/Coin.vue';
import NewsArticle from './pages/NewsArticle';
import PageDefault from './pages/PageDefault';
import PageNotFound from './pages/PageNotFound';

Vue.use(VueRouter);
Vue.use(VueMeta, {
    refreshOnceOnNavigation: true,
});

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/moneta/:id',
            name: 'coin',
            component: Coin,
        },
        {
            path: '/novosti/:id',
            name: 'article',
            component: NewsArticle,
        },
        {
            path: '/:slug',
            name: 'page-default',
            component: PageDefault,
        },
        {
            path: '*',
            component: PageNotFound
        },
    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    },
});

export default router;
