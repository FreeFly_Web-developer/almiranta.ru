/**
 * Укороченная строка
 * @param {string} string Исходная строка
 * @param {number} [start=20] Кол-во символов в начале строки
 * @param {number} [limit=50] Максимальная длина строки
 * @return {string}
 * @private
 */
export default (string, start = 12, limit = 55) => {
    // Задано только стартовое значение, добавить многоточие в конце
    if (string && string.length > start && start >= limit) {
        return `${string.slice(0, limit)}...`;
    }

    // Разбить строку пополам и добавить многоточие внутри
    if (string && string.length > limit && start < limit) {
        return `${string
            .slice(0, start)}...${string
            .slice(-(limit - start))}`;
    }
    return string;
};
