/**
 * Отформатировать число
 * @param {number} num Исходное число
 * @param {number} [dec=2] Число цифр после десятичной точки
 * @param {string} [decPoint=','] Символ десятичной точкой
 * @param {string} [thousandsSep=' '] Символ разделителя разрядов
 * @return {string}
 * @private
 */
export default (num, dec, decPoint = ',', thousandsSep = ' ') => {
    let minus = '';
    let number = num;
    let decimals = 2;

    if (number < 0) {
        minus = '-';
        number *= -1;
    }

    if (!Number.isNaN((Math.abs(dec)))) {
        decimals = dec;
    }
    /* eslint radix: ["error", "as-needed"] */
    const i = `${parseInt((number = (+number || 0).toFixed(decimals)))}`;
    const kw = i.split(/(?=(?:\d{3})+$)/).join(thousandsSep);
    const kd = decimals
        ? decPoint + Math.abs(number - i)
        .toFixed(decimals)
        .replace(/-/, '0')
        .slice(2)
        : '';
    return minus + kw + kd;
};
