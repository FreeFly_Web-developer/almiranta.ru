import Swagger from 'swagger-client';

const specificationUrl = '/openapi_spec';
const specification = Swagger({
    url: specificationUrl,
    requestInterceptor: (req) => {
        const csrf = document.querySelector('meta[name="csrf-token"]');
        if (csrf) {
            req.headers['X-CSRF-TOKEN'] = csrf.getAttribute('content');
        }
        return req;
    },
});

export default specification;