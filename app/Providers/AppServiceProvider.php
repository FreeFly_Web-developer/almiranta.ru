<?php

namespace App\Providers;

use App\Http\Controllers\PageController;
use App\Http\Controllers\SettingsController;
use App\Models\Page;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $phones = [];
        $email = '';
        $telegram = [];
        $script = '';
        $settings = (new SettingsController())->getSettingsList();
        foreach ($settings as $setting) {
            if (!is_array($setting)) {
                if ($setting->name === 'phones') {
                    $phones = $setting->json_body;
                    $phones = array_map(function ($phone) {
                        $phone['formatted'] = str_replace('-', '', str_replace(' ', '', $phone['number']));
                        return $phone;
                    }, $phones);
                } else if ($setting->name === 'email') {
                    $email = $setting->json_body['email'];
                } else if ($setting->name === 'telegram') {
                    $telegram = $setting->json_body;
                } else if ($setting->name === 'rates') {
                    $script = $setting->json_body['script'];
                }
            }
        }
        View::share('pages', (new PageController())->getPagesList());
        View::share('page404', Page::find(6));
        View::share('settings', $settings);
        View::share('email', $email);
        View::share('phones', $phones);
        View::share('telegram', $telegram);
        View::share('script', $script);

        Relation::enforceMorphMap([
          'page'    => 'App\Models\Page',
          'article' => 'App\Models\News',
          'coin'    => 'App\Models\Coin',
        ]);
    }
}
