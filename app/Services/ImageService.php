<?php


namespace App\Services;


class ImageService
{
    // Папки хранения изображений сообразно типам сущности, к которой они относятся
    /** @var string Монеты */
    public const TYPE_COIN         = 'coins';
    /** @var string Новости (главное изображение) */
    public const TYPE_NEWS         = 'news';
    /** @var string Новости (изображения из тела статьи) */
    public const TYPE_NEWS_ARTICLE = 'news-article';
    /** @var string Информационные страницы */
    public const TYPE_PAGE         = 'pages';
    /** @var string Баннеры */
    public const TYPE_BANNER       = 'banners';
    /** @var string Meta Tags */
    public const TYPE_META_TAG     = 'og-image';

    public const PAGE_CONTACTS     = 'contacts-map';
    public const PAGE_WHY_US       = 'why-us';

    public const TYPES_PATHS = [
        self::TYPE_COIN,
        self::TYPE_NEWS,
        self::TYPE_NEWS_ARTICLE,
        self::TYPE_PAGE,
        self::TYPE_BANNER,
        self::TYPE_META_TAG,
    ];

    public const PAGE_TYPES = [
        self::PAGE_CONTACTS => [
            'map-1',
            'map-2',
            'map-3',
        ],
        self::PAGE_WHY_US   => [
            'why-us-main',
            'body',
        ],
    ];

    /** @var string[] Допустимые форматы изображений */
    public const EXTENSIONS = ['jpg', 'png'];

    /** @var string Корневая папка хранения изображений в storage */
    public const BASE_FOLDER = 'public/';


    /**
     * Возвращает массив найденных в переданном тексте путей изображений.
     *
     * @param string $text
     * @return array
     */
    public static function findImagesInText(string $text): array
    {
        preg_match_all(
            '/src="\/storage\/([-a-zA-Z0-9\/]+\.(' . implode('|', self::EXTENSIONS) . '))"/',
            $text,
            $images
        );

        $result = [];
        foreach ($images[1] as $path) {
            $result[] = self::BASE_FOLDER . $path;
        }

        return $result;
    }
}
