<?php


namespace App\Services;


class ToolService
{
    /**
     * Преобразовывает строки с данными в реальные типы данных.
     *
     * Вынужденное решение из-за того, что multipart/form-data отдаёт результаты в виде строк
     *  вне зависимости от реального типа.
     *
     * @param array $values
     * @return array
     */
    public static function convertDataStringsToRealFormats(array $values): array
    {
        foreach ($values as $key=>$value) {
            if (is_numeric(str_replace(',', '.', $value))) {
                $value = str_replace(',', '.', $value);

                if (false === strpos($value, '.')) {
                    $values[$key] = (int) $value;
                }
                else {
                    $values[$key] = (float) $value;
                }
            }
            elseif ('null' == $value) {
                $values[$key] = null;
            }
            elseif ('true' == $value) {
                $values[$key] = true;
            }
            elseif ('false' == $value) {
                $values[$key] = false;
            }
        }

        return $values;
    }
}
