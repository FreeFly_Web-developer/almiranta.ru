<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /** @var string[] "Защищённые" от инъекций поля */
    protected $guarded = ['id'];

    /** @var string[] Типы умолчаний */
    protected $casts = [
        'json_body' => 'array',
    ];
}
