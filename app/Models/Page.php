<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /** @var string[] "Защищённые" от инъекций поля */
    protected $guarded = ['id'];

    /** @var string[] Типы умолчаний */
    protected $casts = [
      'json_body' => 'array',
    ];

    /**
     * The relationships that should always be loaded.
     * @var array
     */
    protected $with = ['meta_tags', 'image_object'];

    /**
     * Get the page's meta tags.
     */
    public function meta_tags()
    {
        return $this->morphMany(MetaTag::class, 'metatagable');
    }

    public function getOgTitleAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogTitle')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgDescriptionAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogDescription')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgImageAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogImage')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgImageWidthAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogImageWidth')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgImageHeightAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogImageHeight')->first();
        }
        return $tag ? $tag->value : null;
    }

    /**
     * Get the page's image.
     */
    public function image_object()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function getImageAltAttribute()
    {
        $imageObject = $this->getAttribute('image_object');
        if ($imageObject) {
            $imageObject = $imageObject->where('name', 'alt')->first();
        }
        return $imageObject ? $imageObject->value : null;
    }
}
