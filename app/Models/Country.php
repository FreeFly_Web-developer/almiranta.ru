<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Country extends Model
{
    /** @var string[] "Защищённые" от инъекций поля */
    protected $guarded = ['id'];

    /** @var string[] Типы умолчаний */
    protected $casts = [
        'isActive'  => 'boolean',
    ];

  /**
   * Get only active countries
   *
   * @param $query
   * @return mixed
   */
    public function scopeActive($query)
    {
        return $query->where('isActive', true);
    }

    /**
     * Each country has many coins
     *
     * @return HasMany
     */
    public function coins(): HasMany
    {
        return $this->hasMany(Coin::class, 'countryId');
    }
}
