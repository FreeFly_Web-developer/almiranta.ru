<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetaTag extends Model
{
    protected $guarded = [];

    /**
     * Get the parent metatagable model
     */
    public function metatagable()
    {
        return $this->morphTo();
    }
}
