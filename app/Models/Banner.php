<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Banner extends Model
{
    /** @var string[] "Защищённые" от инъекций поля */
    protected $guarded = ['id'];

    /** @var string[] Типы умолчаний */
    protected $casts = [
        'isActive'  => 'boolean',
        'order'     => 'integer'
    ];

    /** @var string[] Дополнительные поля */
    protected $appends =[
      'image',
    ];

    /**
     * Get only active banners
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
      return $query->where('isActive', true);
    }

    /**
     * Возвращает картинки для реверса и аверса,
     * если картинка отсутствует - возвращает заглушку
     *
     * @return string
     */
    public function getImageAttribute(): string
    {
      $image = "/storage/banners/$this->id";
      $file = "public/banners/$this->id";
      if (Storage::exists("$file.png")) {
        return "$image.png";
      }
      elseif (Storage::exists("$file.jpg")) {
        return "$image.jpg";
      }
      else {
        return '//via.placeholder.com/343x149';
      }
    }
}
