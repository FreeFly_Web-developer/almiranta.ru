<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;


class Coin extends Model
{
    public const CONDITION_UNCIRCULATED             = 3;
    public const CONDITION_BRILLIANT_UNCIRCULATED   = 4;
    public const CONDITION_PROOF                    = 5;

    public const METAL_GOLD       = 1;
    public const METAL_PLATINUM   = 2;
    public const METAL_SILVER     = 3;

    public const CONDITIONS = [
        self::CONDITION_UNCIRCULATED            => 'UNC',
        self::CONDITION_BRILLIANT_UNCIRCULATED  => 'BU',
        self::CONDITION_PROOF                   => 'PROOF',
    ];

    public const METALS = [
        self::METAL_GOLD        => 'Золото',
        self::METAL_PLATINUM    => 'Платина',
        self::METAL_SILVER      => 'Серебро',
    ];

    /**
     * The relationships that should always be loaded.
     * @var array
     */
    protected $with = ['meta_tags', 'image_object'];


    /** @var string[] "Защищённые" от инъекций поля */
    protected $guarded = ['id'];

    /** @var string[] Типы умолчаний */
    protected $casts = [
        'weight'                => 'float',
        'weightPure'            => 'float',
        'metalId'               => 'integer',
        'purity'                => 'integer',
        'diameter'              => 'float',
        'thickness'             => 'float',
        'year'                  => 'integer',
        'periodFrom'            => 'integer',
        'periodTo'              => 'integer',
        'mintage'               => 'integer',
        'countryId'             => 'integer',
        'isActive'              => 'boolean',
        'conditionId'           => 'integer',
        'priceBuy'              => 'float',
        'priceSell'             => 'float',
        'nofMinDiscussPrice'    => 'float',
        'nofAvailable'          => 'integer',
        'order'                 => 'integer',
    ];

    /** @var string[] Дополнительные поля */
    protected $appends = [
        'images',
        'metal',
        'countryName',
        'condition',
    ];

    public function scopeActive($query)
    {
        return $query->where('isActive', true);
    }

  /**
   * Возвращает картинки для реверса и аверса,
   * если картинка отсутствует - возвращает заглушку
   *
   * @return string[]
   */
    public function getImagesAttribute(): array
    {
        $images = [];
        foreach ([1, 2] as $index) {
          $image = "/storage/coins/$this->id-$index";
          $file = "public/coins/$this->id-$index";
          if (Storage::exists("$file.png")) {
            $images[] = "$image.png";
          } elseif (Storage::exists("$file.jpg")) {
            $images[] = "$image.jpg";
          } else {
            $images[] = '//via.placeholder.com/232x232';
          }
        }

        return $images;
    }


    /**
     * Возвращает название металла по заданному ID
     *
     * @return string[]
     */
    public function getMetalAttribute(): array
    {
        $metals = [];

        foreach (self::METALS as $id => $name) {
            $metals[] = [
                'id'    => $id,
                'name'  => $name,
            ];
        }

        $metals = collect($metals);

        return $metals->where('id', $this->getAttribute('metalId'))->first() ?? $metals[0];
    }

    /**
     * Возвращает название состояния по заданному ID
     *
     * @return string[]
     */
    public function getConditionAttribute(): array
    {
        $conditions = [];

        foreach (self::CONDITIONS as $id => $name) {
            $conditions[] = [
                'id'    => $id,
                'name'  => $name,
            ];
        }

        $conditions = collect($conditions);

        return $conditions->where('id', $this->getAttribute('conditionId'))->first() ?? $conditions[0];
    }

    public function getCountryNameAttribute()
    {
        return $this->getAttribute('country')->name;
    }

    /**
     * Get the country that owns the coin.
     *
     * @return BelongsTo
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'countryId');
    }

    public function getTypeAttribute()
    {
        return 'coin';
    }

    /**
     * Get the coin's meta tags.
     */
    public function meta_tags()
    {
        return $this->morphMany(MetaTag::class, 'metatagable');
    }

    public function getOgTitleAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogTitle')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgDescriptionAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogDescription')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgImageAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogImage')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgImageWidthAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogImageWidth')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgImageHeightAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogImageHeight')->first();
        }
        return $tag ? $tag->value : null;
    }

    /**
     * Get the coin's image.
     */
    public function image_object()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function getImageAltAttribute()
    {
        $imageObject = $this->getAttribute('image_object');
        $imageObject1 = $imageObject->where('name', 'alt_1')->first();
        $imageObject2 = $imageObject->where('name', 'alt_2')->first();
        return [
          1 => $imageObject1 ? $imageObject1->value : '',
          2 => $imageObject2 ? $imageObject2->value : '',
        ];
    }
}
