<?php

namespace App\Models;

use App\Services\ImageService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class News extends Model
{
    /** @var string[] "Защищённые" от инъекций поля */
    protected $guarded = ['id'];

    /** @var string[] Типы умолчаний */
    protected $casts = [
      'isActive'    => 'boolean',
    ];

    /** @var string[] Дополнительные поля */
    protected $appends =[
        'image',
        'shortText',
    ];

    /**
     * The relationships that should always be loaded.
     * @var array
     */
    protected $with = ['meta_tags', 'image_object'];


  /**
   * Get only active news
   *
   * @param $query
   * @return mixed
   */
    public function scopeActive($query)
    {
        return $query->where('isActive', true);
    }

    /**
     * Возвращает картинку для новостной статьи.
     * Если картинка отсутствует - возвращает заглушку.
     *
     * @return string
     */
    public function getImageAttribute(): string
    {
        $image = "/storage/news/$this->id";
        $file = "public/news/$this->id";
        if (Storage::exists("$file.png")) {
            return "$image.png";
        }
        elseif (Storage::exists("$file.jpg")) {
            return "$image.jpg";
        }
        else {
            return '//via.placeholder.com/443x296';
        }
    }

    /**
     * Returns text preview
     *
     * @return string
     */
    public function getShortTextAttribute(): string
    {
        return Str::limit(strip_tags($this->getAttribute('text')), 190);
    }

    public function getTypeAttribute()
    {
        return 'news-article';
    }

    /**
     * Get the article's meta tags.
     */
    public function meta_tags()
    {
        return $this->morphMany(MetaTag::class, 'metatagable');
    }

    public function getOgTitleAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogTitle')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgDescriptionAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogDescription')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgImageAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogImage')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgImageWidthAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogImageWidth')->first();
        }
        return $tag ? $tag->value : null;
    }

    public function getOgImageHeightAttribute()
    {
        $tag = $this->getAttribute('meta_tags');
        if ($tag) {
            $tag = $tag->where('name', 'ogImageHeight')->first();
        }
        return $tag ? $tag->value : null;
    }

    /**
     * Get the article's image.
     */
    public function image_object()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function getImageAltAttribute()
    {
        $imageObject = $this->getAttribute('image_object');
        if ($imageObject) {
            $imageObject = $imageObject->where('name', 'alt')->first();
        }
        return $imageObject ? $imageObject->value : null;
    }
}
