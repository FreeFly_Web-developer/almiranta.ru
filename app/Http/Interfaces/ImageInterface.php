<?php

namespace App\Http\Interfaces;

use App\Http\Controllers\Image\ImageHandler;
use Illuminate\Http\Request;

interface ImageInterface
{
    public function __construct(ImageHandler $imageHandler);

    public function uploadImages(Request $request): array;

    public function deleteImages(Request $request): array;
}