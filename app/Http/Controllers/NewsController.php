<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\News;
use App\Services\ImageService;
use App\Services\ToolService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class NewsController extends Controller
{
    private const PAGE_DEFAULT   = 1;
    private const LIMIT_DEFAULT  = 12;

    public function __construct()
    {
        $this->middleware(['auth:sanctum'], ['except' => [
            'getNewsArticleBySlug', 'getNewsList',
        ]]);
    }

    public function getNewsList(Request $request): array
    {
        $page = $request->get('page') ?? self::PAGE_DEFAULT;
        $limit = $request->get('limit') ?? self::LIMIT_DEFAULT;
        $isActiveOnly = (!in_array($request->get('isActiveOnly'), ["0", "false"], true)
          ? $request->get('isActiveOnly') : false);

        return Cache::remember("news-$page-$limit-$isActiveOnly", now()->addWeek(),
          function() use ($page, $limit, $isActiveOnly) {
            $news = new News();

            if ($isActiveOnly) {
                $news = $news->active();
            }
            $news = $news->where('releasedAt', '<=', now());
            $total = $news->count();

            if ($limit) {
                $news = $news->limit($limit)->offset(abs($page - 1) * $limit);
            }

            return [
              'news' => $news->with('image_object')->orderByDesc('releasedAt')->get(),
              'page' => $page,
              'limit' => $limit,
              'nofTotal' => $total,
            ];
        });
    }

    public function getNewsArticleById(News $newsArticleId): News
    {
        return $newsArticleId;
    }

    public function getNewsArticleBySlug(string $slug): News
    {
        return Cache::remember("news-$slug", now()->addWeek(), function() use ($slug) {
            return $this->getNewsArticleById(News::active()->where('slug', $slug)->with(['meta_tags', 'image_object'])->firstOrFail());
        });
    }

    public function addNewsArticle(Request $request, ImageController $imageController): array
    {
        $data = ToolService::convertDataStringsToRealFormats($request->all());
        $this->validateRequest($data);
        $data['slug'] = Str::slug($request->get('name'));
        $data['type'] = ImageService::TYPE_NEWS;

        $news = new News($data);
        $news->metaTitle = $news->metaTitle ?: $news->name;
        $news->releasedAt = $news->releasedAt ?: now()->format('Y-m-d H:i:s');
        $news->save();
        $data['id'] = $news->id;
        $news->text = str_replace('/0/', "/$news->id/", $news->text);
        (new MetaTagController($news, json_decode($request->get('meta_tags'), true)))->save();
        $news->save();
        $news->image_object()->save(new Image(['name' => 'alt', 'value' => $request->get('image_alt')]));

        $imageController->uploadImages($request->merge($data));

        $imageController->cleanUpDirectory($news->id, $news->text,
          ImageService::BASE_FOLDER . ImageService::TYPE_NEWS_ARTICLE, true);
        Cache::flush();

        return [
            'id'        => $news->id,
            'message'   => 'Новость успешно добавлена!',
        ];
    }

    public function updateNewsArticle(News $newsArticleId, Request $request, ImageController $imageController): array
    {
        $this->validateRequest($request->all());

        foreach ($request->except(['image', 'shortText', 'created_at', 'updated_at',
          'meta_tags', 'image_object', 'image_alt']) as $name => $value) {
            $newsArticleId->$name = $value;
        }

        $newsArticleId->slug = Str::slug($newsArticleId->name);
        (new MetaTagController($newsArticleId, $request->get('meta_tags')))->save();
        $newsArticleId->image_object()->delete();
        $newsArticleId->image_object()->save(new Image(['name' => 'alt', 'value' => $request->get('image_alt')]));

        $imageController->cleanUpDirectory($newsArticleId->id, $newsArticleId->text,
          ImageService::BASE_FOLDER . ImageService::TYPE_NEWS_ARTICLE);

        $newsArticleId->save();
        Cache::flush();

        return [
          'message' => 'Новость успешно обновлена!',
        ];
    }

    public function deleteNewsArticle(News $newsArticleId, Request $request, ImageController $imageController): array
    {
        $imageController->deleteImages($request->merge(['id' => $newsArticleId->id, 'type' => ImageService::TYPE_NEWS]));
        $imageController->deleteImages($request->merge(['id' => $newsArticleId->id, 'type' => ImageService::TYPE_NEWS_ARTICLE]));
        $newsArticleId->delete();
        Cache::flush();

        return [
            'message' => 'Новость успешно удалена!',
        ];
    }

    private function validateRequest(array $data): array
    {
        return Validator::make($data, [
          'name'            => [
            'sometimes',
            'required',
            'string',
            'between:2,255',
            Rule::unique('news')->ignore($data['id']),
          ],
          'text'            => 'sometimes|required|string|min:2',
          'isActive'        => 'required|boolean',
          'metaTitle'       => 'nullable|between:2,255',
          'metaDescription' => 'nullable|between:2,255',
          'ogTitle'         => 'nullable|between:2,255',
          'ogDescription'   => 'nullable|between:2,255',
          'ogImageWidth'    => 'nullable|number',
          'ogImageHeight'   => 'nullable|number',
        ])->validate();
    }
}
