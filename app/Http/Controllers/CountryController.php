<?php

namespace App\Http\Controllers;

use App\Models\Coin;
use App\Models\Country;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum'], ['except' => [
            'getCountriesList',
        ]]);
    }

    public function getCountriesList(Request $request)
    {
        if ($request->filled('isActiveOnly')) {
            return Country::active()->get();
        }

        return Country::all();
    }

    public function addCountry(Request $request)
    {
        $this->validateRequest($request);

        $countryNew = Country::create($request->only(['name', 'isActive']));

        return [
          'id'        => $countryNew->id,
          'message'   => 'Страна успешно добавлена!',
        ];
    }

    public function getCountry(Country $countryId)
    {
        return $countryId;
    }

    public function updateCountry(Country $countryId, Request $request)
    {
        throw_if($countryId->id === 1, new Exception('Данный пункт нельзя изменить!', 422));

        $this->validateRequest($request, $countryId);

        if ($request->filled('name')) {
          $countryId->name = $request->get('name');
        }
        $countryId->isActive = $request->get('isActive');
        $countryId->save();

        return [
          'message' => 'Параметры страны успешно обновлены!',
        ];
    }

    public function deleteCountry(Country $countryId)
    {
        throw_if($countryId->id === 1, new Exception('Данный пункт нельзя удалить!', 422));

        Coin::where('countryId', $countryId->id)->update(['countryId' => 1]);
        $countryId->delete();

        return [
          'message' => 'Страна успешно удалена!',
        ];
    }

    private function validateRequest(Request $request, Country $countryId = null)
    {
        $nameRules = [
          'sometimes',
          'required',
          'string',
          'between:2,255',
        ];

        if ($countryId) {
          $nameRules[] = Rule::unique('countries')->ignore($countryId->id);
        }

        $request->validate([
          'name'     => $nameRules,
          'isActive' => 'required|boolean',
        ]);
    }
}
