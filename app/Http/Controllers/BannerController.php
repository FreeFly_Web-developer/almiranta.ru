<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Services\ImageService;
use App\Services\ToolService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    public function __construct()
    {
          $this->middleware(['auth:sanctum'], ['except' => [
            'getBannersList', 'getBanner',
          ]]);
    }

    public function getBannersList(Request $request)
    {
        $orderField = $request->get('orderField');
        $orderDirection = $request->get('orderDirection');
        $isActiveOnly = $request->filled('isActiveOnly');

        return Cache::remember("banners-$orderField-$orderDirection-$isActiveOnly", now()->addWeek(),
          function () use ($orderField, $orderDirection, $isActiveOnly) {
            $banners = new Banner();

            if (in_array($orderField, ['order', 'id'])) {
                $banners = $banners->orderBy($orderField,
                  (in_array($orderDirection, ['asc', 'desc']) ? $orderDirection : 'asc'));
            }

            if ($isActiveOnly) {
                return $banners->active()->get();
            }

            return $banners->get();
        });
    }

    public function getBanner(Banner $bannerId): Banner
    {
        return $bannerId;
    }

    public function addBanner(Request $request, ImageController $imageController): array
    {
        $data = ToolService::convertDataStringsToRealFormats($request->all());
        $this->validateRequest($data);
        $data['type'] = ImageService::TYPE_BANNER;

        $bannerNew = new Banner($data);
        $bannerNew->order = $bannerNew->order ?: 0;
        $bannerNew->save();
        $data['id'] = $bannerNew->id;

        $imageController->uploadImages($request->merge($data));
        Cache::flush();

        return [
            'id'        => $bannerNew->id,
            'message'   => 'Баннер успешно добавлена!',
        ];
    }

    public function updateBanner(Banner $bannerId, Request $request, ImageController $imageController): array
    {
        $data = ToolService::convertDataStringsToRealFormats($request->all());
        $this->validateRequest($data);
        $data['image'] = !is_string($data['image']) ?: null;
        $data['type'] = ImageService::TYPE_BANNER;

        $imageController->uploadImages($request->merge($data));

        foreach ($request->except(['image', '_method', 'type']) as $name => $value) {
            $bannerId->$name = $value;
        }

        $bannerId->save();
        Cache::flush();

        return [
            'message' => 'Параметры баннера успешно обновлены!',
        ];
    }

    public function deleteBanner(Banner $bannerId, Request $request, ImageController $imageController): array
    {
        $imageController->deleteImages($request->merge(['id' => $bannerId->id, 'type' => ImageService::TYPE_BANNER]));
        $bannerId->delete();
        Cache::flush();

        return [
          'message' => 'Баннер успешно удалён!',
        ];
    }

    private function validateRequest(array $data): array
    {
        return Validator::make($data, [
          'title'     => 'required|between:2,255',
          'isActive'  => 'required|boolean',
          'url'       => 'nullable|url',
          'text'      => 'nullable|string',
          'order'     => 'nullable|integer',
        ])->validate();
    }
}
