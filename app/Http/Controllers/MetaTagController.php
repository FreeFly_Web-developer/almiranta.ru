<?php

namespace App\Http\Controllers;

use App\Models\MetaTag;
use Illuminate\Database\Eloquent\Model;

class MetaTagController extends Controller
{
    private $model;
    private $metaTags;

    public function __construct(Model $model, array $metaTags)
    {
        $this->middleware(['auth:sanctum']);
        $this->model = $model;
        $this->metaTags = $metaTags;
    }

    public function save()
    {
        $metaTags = [];
        foreach ($this->metaTags as $metaTag => $value) {
            $metaTags[] = new MetaTag([
              'name'  => $metaTag,
              'value' => $value,
            ]);
        }

        $this->model->meta_tags()->delete();
        $this->model->meta_tags()->saveMany($metaTags);
    }
}