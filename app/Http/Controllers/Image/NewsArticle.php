<?php

namespace App\Http\Controllers\Image;

use App\Http\Interfaces\ImageInterface;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsArticle implements ImageInterface
{
    protected $imageHandler;
    protected $image = 'image';

    private $folder;

    public function __construct(ImageHandler $imageHandler)
    {
        $this->imageHandler = $imageHandler;
        $this->folder = ImageService::BASE_FOLDER . ImageService::TYPE_NEWS_ARTICLE;
    }

    public function uploadImages(Request $request): array
    {
        $this->folder .= '/' . ($request->get('id') ?: 0);
        $image = $this->image;
        $data = [];

        if ($request->$image) {
            $data[$image] = [
              'file'      => $request->$image,
              'name'      => time(),
              'extension' => $request->$image->extension(),
            ];
        }

        return $this->imageHandler->upload($this->folder, $data);
    }

    public function deleteImages(Request $request): array
    {
        $this->folder .= '/' . $request->get('id');
        $files = collect(Storage::allFiles($this->folder))->map(function ($item) {
            preg_match_all('/\w*(?=\.)/', $item, $file);
            return $file;
        })->flatten()->filter(function ($item) {
            return $item;
        })->toArray();
        return $this->imageHandler->delete($this->folder, $files);
    }
}