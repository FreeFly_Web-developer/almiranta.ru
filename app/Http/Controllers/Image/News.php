<?php

namespace App\Http\Controllers\Image;

use App\Http\Interfaces\ImageInterface;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class News implements ImageInterface
{
    protected $imageHandler;
    protected $image = 'image';

    private $folder;
    private $rule;

    public function __construct(ImageHandler $imageHandler)
    {
        $this->imageHandler = $imageHandler;
        $this->folder = ImageService::BASE_FOLDER . ImageService::TYPE_NEWS;
        $this->rule = Rule::dimensions()->minWidth(443)->minHeight(296);
    }

    public function uploadImages(Request $request): array
    {
        $image = $this->image;
        $rules = [];
        $data = [];

        if ($request->$image) {
            $rules[$image][] = $this->rule;
            $data[$image] = [
              'file'      => $request->$image,
              'name'      => ($request->get('type') !== 'ogImage' ? $request->get('id') : $request->get('image_name')),
              'extension' => $request->$image->extension(),
            ];
        }

        return $this->imageHandler->upload($this->folder, $data, $rules);
    }

    public function deleteImages(Request $request): array
    {
        return $this->imageHandler->delete($this->folder, [$request->get('id')]);
    }
}