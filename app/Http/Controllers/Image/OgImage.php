<?php

namespace App\Http\Controllers\Image;

use App\Http\Interfaces\ImageInterface;
use App\Services\ImageService;
use Illuminate\Http\Request;

class OgImage implements ImageInterface
{
    protected $imageHandler;
    protected $image = 'imageOgImage';

    private $folder;

    public function __construct(ImageHandler $imageHandler)
    {
        $this->imageHandler = $imageHandler;
        $this->folder = ImageService::BASE_FOLDER . ImageService::TYPE_META_TAG;
    }

    public function uploadImages(Request $request): array
    {
        $image = $this->image;
        $data = [];

        if ($request->$image) {
            $data[$image] = [
              'file'      => $request->$image,
              'name'      => ($request->get('type') !== 'og-image' ? $request->get('id') : $request->get('image_name')),
              'extension' => $request->$image->extension(),
            ];
        }

        return $this->imageHandler->upload($this->folder, $data);
    }

    public function deleteImages(Request $request): array
    {
        return $this->imageHandler->delete($this->folder, [$request->get('id')]);
    }
}