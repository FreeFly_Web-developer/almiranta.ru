<?php

namespace App\Http\Controllers\Image;

use App\Http\Interfaces\ImageInterface;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class Pages implements ImageInterface
{
    private $imageHandler;
    private $folder;
    private $image = 'image';
    private $rule;

    public function __construct(ImageHandler $imageHandler)
    {
        $this->imageHandler = $imageHandler;
        $this->folder = ImageService::BASE_FOLDER . ImageService::TYPE_PAGE;
        $this->rule = Rule::dimensions()->minWidth(317)->minHeight(317);
    }

    public function uploadImages(Request $request): array
    {
        $image = $this->image;
        $rules = [];
        $data = [];

        if ($request->$image) {
            $rules[$image][] = $this->rule;
            $data[$image] = [
              'file'      => $request->$image,
              'name'      => ($request->get('image_name')
              ?: Str::slug(explode('.', $request->$image->getClientOriginalName())[0])),
              'extension' => 'png',
            ];
        }

        return $this->imageHandler->upload($this->folder, $data, $rules);
    }

    public function deleteImages(Request $request): array
    {
        $this->folder .= '/' . $request->get('id');
        $files = collect(Storage::allFiles($this->folder))->map(function ($item) {
            preg_match_all('/\w*(?=\.)/', $item, $file);
            return $file;
        })->flatten()->filter(function ($item) {
            return $item;
        })->toArray();
        return $this->imageHandler->delete($this->folder, $files);
    }
}