<?php

namespace App\Http\Controllers\Image;

use App\Http\Interfaces\ImageInterface;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class Coins implements ImageInterface
{
    protected $imageHandler;
    protected $images = [1 => 'imageObverse', 2 => 'imageReverse'];

    private $folder;
    private $rule;

    public function __construct(ImageHandler $imageHandler)
    {
        $this->imageHandler = $imageHandler;
        $this->folder = ImageService::BASE_FOLDER . ImageService::TYPE_COIN;
        $this->rule = Rule::dimensions()->minWidth(100)->minHeight(100);
    }

    public function uploadImages(Request $request): array
    {
        $rules = [];
        $data = [];

        foreach ($this->images as $side => $image) {
            if ($request->$image) {
                $rules[$image][] = $this->rule;
                $data[$image] = [
                  'file'      => $request->$image,
                  'name'      => $request->get('id') . '-' . $side,
                  'extension' => $request->$image->extension(),
                  'fit'       => true,
                ];
            }
        }

        return $this->imageHandler->upload($this->folder, $data, $rules);
    }

    public function deleteImages(Request $request): array
    {
        $id = $request->get('id');
        $images = [];
        foreach (array_keys($this->images) as $side) {
            $images[] = "$id-$side";
        }

        return $this->imageHandler->delete($this->folder, $images);
    }
}