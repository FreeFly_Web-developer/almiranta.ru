<?php

namespace App\Http\Controllers\Image;

use App\Services\ImageService;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ImageHandler
{
    private $rules = [
      'nullable',
      'image',
      'mimes:jpeg,jpg,png',
      'max:5196',
    ];

    public function upload(string $folder, array $data, array $rules = []): array
    {
        Validator::make(array_map(function ($item) { return $item['file']; }, $data), $this->addRules($rules))->validate();

        $response = $this->delete($folder, array_column($data, 'name'));
        throw_if(!$response['ok'], $response);

        if (!file_exists(storage_path("app/$folder"))) {
            Storage::makeDirectory($folder);
        }

        try {
            $paths = [];
            foreach ($data as $image) {
                $filename = $image['name'] . '.' . $image['extension'];
                $paths[$image['name']] = '/storage/' . str_replace('public/', '', $folder) . '/' . $filename;
                $imageFile = Image::make($image['file']);
                if (!array_key_exists('fit', $image)) {
//                    $imageSideSize = min($imageFile->height(), 500);
//                    $imageFile->resize(null, $imageSideSize, function ($constraint) {
//                        $constraint->aspectRatio();
//                    });
                } else {
                    $imageSideSize = min($imageFile->height(), $imageFile->width());
                    $imageFile->fit($imageSideSize, $imageSideSize);
                }

                $imageFile->save(storage_path("app/$folder/$filename"));
            }

            return [
              'ok'      => true,
              'paths'   => $paths,
              'message' => 'Картинки успешно загружены',
            ];
        } catch (Exception $exception) {
            return [
              'ok'      => false,
              'line'    => $exception->getLine(),
              'file'    => $exception->getFile(),
              'errors'  => $exception->getMessage(),
              'message' => 'Ошибка во время загрузки картинок!',
            ];
        }
    }

    public function delete(string $folder, array $images): array
    {
        try {
            foreach ($images as $image) {
                foreach (ImageService::EXTENSIONS as $extension) {
                    if (Storage::exists("$folder/$image.$extension")) {
                        Storage::delete("$folder/$image.$extension");
                    }
                }
            }
            if (empty(Storage::allFiles($folder))) {
                Storage::deleteDirectory($folder);
            }
            return [
              'ok'  => true,
              'message' => 'Картинки успешно удалены!',
            ];
        } catch (Exception $exception) {
            return [
              'ok'      => false,
              'line'    => $exception->getLine(),
              'file'    => $exception->getFile(),
              'errors'  => $exception->getMessage(),
              'message' => 'Ошибка во время удаления картинок!',
            ];
        }
    }

    private function addRules(array $rules): array
    {
        return array_map(function ($rule) {
            return array_merge($rule, $this->rules);
        }, $rules);
    }
}