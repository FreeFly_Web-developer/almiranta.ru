<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Coin;
use App\Models\News;

class HomeController extends Controller
{
    public function index()
    {
        return view('pages.home', [
          'banners'  => (new BannerController())->getBannersList(request()->merge(['isActiveOnly' => true, 'orderField' => 'order'])),
          'coins'    => (new CoinController())->getCoinsList(request()->merge(['limit' => 0, 'isActiveOnly' => true, 'orderField' => 'order'])),
          'articles' => (new NewsController())->getNewsList(request()->merge([
            'limit' => 3,
            'isActiveOnly' => true
          ])),
        ]);
    }
}
