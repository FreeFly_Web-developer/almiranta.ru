<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Image\ImageHandler;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Interfaces\ImageInterface;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Exception;

class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function uploadImages(Request $request): array
    {
        Validator::make($request->only(['id', 'type']), [
          'id'    => 'required|integer',
          'type'  => [
            'required',
            Rule::in(ImageService::TYPES_PATHS),
          ],
        ])->validate();
        if ($request->get('type') === 'og-image') {
            $request->merge([
              'image_name' => Str::slug(explode('.', $request->file('imageOgImage')->getClientOriginalName())[0]),
            ]);
        }

        return $this->upload($request, $this->getType($request->get('type')));
    }

    private function upload(Request $request, ImageInterface $imageInterface): array
    {
        return $imageInterface->uploadImages($request);
    }

    public function deleteImages(Request $request): array
    {
        Validator::make($request->only(['id', 'type']), [
          'id'   => 'required|integer|min:1',
          'type' => [
            'required',
            Rule::in(ImageService::TYPES_PATHS),
          ],
        ])->validate();
        
        return $this->delete($request, $this->getType($request->get('type')));
    }

    private function delete(Request $request, ImageInterface $imageInterface): array
    {
        return $imageInterface->deleteImages($request);
    }

    public function cleanUpDirectory(int $id, string $text, string $folder, bool $isNew = false): void
    {
        if ($isNew && Storage::exists("$folder/0")) {
            Storage::move("$folder/0", "$folder/$id");
        }

        $imagesUpdated = ImageService::findImagesInText($text);
        $allImages = Storage::allFiles("$folder/$id");
        $imagesToDelete = array_diff($allImages, $imagesUpdated);

        foreach ($imagesToDelete as $imagePath) {
            Storage::delete($imagePath);
        }
    }

    private function getType(string $type)
    {
        $class = implode('', array_map(function ($item) { return ucfirst($item); }, explode('-', $type)));
        $class = "App\Http\Controllers\Image\\$class";
        throw_if(!class_exists($class), new Exception('Отсутствует необходимый загрузчик'));
        return new $class(new ImageHandler());
    }
}