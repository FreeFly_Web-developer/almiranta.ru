<?php

namespace App\Http\Controllers;

use App\Models\Coin;
use App\Models\Image;
use App\Services\ImageService;
use App\Services\ToolService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class CoinController extends Controller
{
    private const PAGE_DEFAULT  = 1;
    private const LIMIT_DEFAULT = 12;

    public const COINS_PARAMETERS = [
      [
        'name' => 'metal',
        'title' => 'Драгоценный металл',
      ],
      [
        'name' => 'weight',
        'title' => 'Общий вес',
        'postfix' => 'г',
      ],
      [
        'name' => 'purity',
        'title' => 'Проба металла',
      ],
      [
        'name' => 'weightPure',
        'title' => 'Чистого драгметалла',
        'postfix' => 'г',
      ],
      [
        'name' => 'countryName',
        'title' => 'Страна эмитент монеты',
      ],
      [
        'name' => 'denomination',
        'title' => 'Номинал монеты',
      ],
      [
        'name' => 'denominationCurrency',
        'title' => 'Валюта номинала',
      ],
      [
        'name' => 'year',
        'title' => 'Год выпуска',
      ],
      [
        'name' => 'diameter',
        'title' => 'Диаметр',
        'postfix' => 'мм',
      ],
      [
        'name' => 'thickness',
        'title' => 'Толщина',
        'postfix' => 'мм',
      ],
      [
        'name' => 'condition',
        'title' => 'Качество чеканки монеты',
      ],
    ];

    public function __construct()
    {
        $this->middleware(['auth:sanctum'], ['except' => [
            'getCoinsList', 'getCoinBySlug',
        ]]);
    }

    public function getCoinsList(Request $request): array
    {
        $page = $request->get('page') ?? self::PAGE_DEFAULT;
        $limit = $request->get('limit') ?? self::LIMIT_DEFAULT;
        $orderField = $request->get('orderField');
        $orderDirection = $request->get('orderDirection');
        $isActiveOnly = (!in_array($request->get('isActiveOnly'), ["0", "false"], true)
          ? $request->get('isActiveOnly') : false);

        return Cache::remember("coins-$page-$limit-$orderField-$orderDirection-$isActiveOnly", now()->addWeek(),
          function() use ($page, $limit, $orderField, $orderDirection, $isActiveOnly) {
              $coins = new Coin();
            if (in_array($orderField, ['order', 'id'])) {
                $coins = $coins->orderBy($orderField,
                  (in_array($orderDirection, ['asc', 'desc']) ? $orderDirection : 'asc'));
            }

            if ($isActiveOnly) {
                $coins = $coins->active();
            }

            if ($limit) {
                $coins = $coins->limit($limit)->offset(abs($page - 1) * $limit);
            }

            return [
              'coins' => $coins->with('image_object')->get(),
              'page' => $page,
              'limit' => $limit,
              'nofTotal' => $coins->count(),
            ];
        });
    }

    public function getCoinById(Coin $coinId): Coin
    {
        return $coinId;
    }

    public function getCoinBySlug(string $slug): Coin
    {
        return Cache::remember("coin-$slug", now()->addWeek(), function() use ($slug) {
            return $this->getCoinById(Coin::active()->where('slug', $slug)->with(['meta_tags', 'image_object'])->firstOrFail());
        });
    }

    public function addCoin(Request $request, ImageController $imageController): array
    {
        $data = ToolService::convertDataStringsToRealFormats($request->all());
        $this->validateRequest($data);
        $data['slug'] = Str::slug($request->get('name'));
        $data['type'] = ImageService::TYPE_COIN;

        $coinNew = new Coin($data);
        $coinNew->order = $coinNew->order ?: 0;
        $coinNew->metaTitle = $coinNew->metaTitle ?: $coinNew->name;
        $coinNew->save();
        $data['id'] = $coinNew->id;
        (new MetaTagController($coinNew, $request->get('meta_tags')))->save();
        $coinNew->image_object()->saveMany([
          new Image(['name' => 'alt_1', 'value' => $request->get('image_alt_1')]),
          new Image(['name' => 'alt_2', 'value' => $request->get('image_alt_2')])
        ]);

        $imageController->uploadImages($request->merge($data));
        Cache::flush();

        return [
          'id'        => $coinNew->id,
          'message'   => 'Монета успешно добавлена!',
        ];
    }

    public function updateCoin(Coin $coinId, Request $request): array
    {
        $this->validateRequest($request->all());

        $exceptedFields = [
            'images', 'imageObverse', 'imageReverse', 'metal', 'condition', 'countryName', 'country',
            'meta_tags', 'image_object', 'image_alt_1', 'image_alt_2',
            'created_at', 'updated_at',
        ];

        foreach ($request->except($exceptedFields) as $name => $value) {
            $coinId->$name = $value;
        }

        $coinId->slug = Str::slug($coinId->name);
        (new MetaTagController($coinId, $request->get('meta_tags')))->save();
        $coinId->image_object()->delete();
        $coinId->image_object()->saveMany([
          new Image(['name' => 'alt_1', 'value' => $request->get('image_alt_1')]),
          new Image(['name' => 'alt_2', 'value' => $request->get('image_alt_2')])
        ]);

        $coinId->save();
        Cache::flush();

        return [
            'message' => 'Параметры монеты успешно обновлены!',
        ];
    }

    public function deleteCoin(Coin $coinId, Request $request, ImageController $imageController): array
    {
        $imageController->deleteImages($request->merge(['id' => $coinId->id, 'type' => ImageService::TYPE_COIN]));
        $coinId->delete();
        Cache::flush();

        return [
            'message' => 'Монета успешно удалена!',
        ];
    }

    private function validateRequest(array $data): array
    {
        return Validator::make($data, [
          'name'          => [
            'sometimes',
            'required',
            'string',
            'between:2,255',
            Rule::unique('coins')->ignore($data['id']),
          ],
          'description'   => 'nullable|min:2',
          'weight'        => 'sometimes|required|numeric',
          'weightPure'    => 'sometimes|required|numeric',
          'metalId'       => [
            'sometimes',
            'required',
            Rule::in(array_keys(Coin::METALS)),
          ],
          'purity'        => 'sometimes|required|integer|between:0,100000',
          'diameter'      => 'nullable|numeric',
          'thickness'     => 'nullable|numeric',
          'year'          => 'nullable|date_format:"Y"',
          'periodFrom'    => 'nullable|date_format:"Y"',
          'periodTo'      => 'nullable|date_format:"Y"',
          'isActive'      => 'required|boolean',
          'mintage'       => 'nullable|integer',
          'countryId'     => 'exists:countries,id',
          'denomination'  => 'sometimes|required|max:255',
          'denominationCurrency' => 'sometimes|required|max:255',
          'mint'          => 'nullable|max:255',
          'conditionId'   => [
            'sometimes',
            'required',
            Rule::in(array_keys(Coin::CONDITIONS)),
          ],
          'priceBuy'              => 'nullable|numeric',
          'priceSell'             => 'nullable|numeric',
          'nofMinDiscussPrice'    => 'nullable|numeric',
          'nofAvailable'          => 'nullable|integer',
          'order'                 => 'nullable|integer',
          'metaTitle'             => 'nullable|between:2,255',
          'metaDescription'       => 'nullable|between:2,255',
          'ogTitle'               => 'nullable|between:2,255',
          'ogDescription'         => 'nullable|between:2,255',
          'ogImageWidth'          => 'nullable|number',
          'ogImageHeight'         => 'nullable|number',
        ])->validate();
    }
}
