<?php

namespace App\Http\Controllers;

use App\Models\Coin;
use App\Models\News;
use App\Models\Page;
use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    public function __construct()
    {
      $this->middleware(['auth:sanctum'], ['except' => [
        'getSettingsList',
      ]]);
    }

    public function getSettingsList()
    {
        return Cache::remember('settings', now()->addWeek(), function () {
            $metals = collect(Coin::METALS)->map(function ($item, $key) {
                return [
                  'id' => $key,
                  'name' => $item,
                ];
            });
            $conditions = collect(Coin::CONDITIONS)->map(function ($item, $key) {
                return [
                  'id' => $key,
                  'name' => $item,
                ];
            });
            $settings = Settings::where('name', '!=', 'events')->get();
            $settings->push([
              'id'   => $settings->max('id') + 1,
              'name' => 'coins',
              'json_body' => [
                'metals'     => $metals->toArray(),
                'conditions' => $conditions->toArray(),
              ],
            ]);

            return $settings;
        });
    }

    public function updateSettingsItem(Settings $settingsItemId, Request $request)
    {
        $request->validate([
          'json_body' => 'required|array',
        ]);

        $settingsItemId->json_body = $request->get('json_body');
        $settingsItemId->save();

        if ($settingsItemId->name === 'robots') {
            Storage::disk('public_local')->put('robots.txt', $request->get('json_body')['content']);
        }

        if ($settingsItemId->name === 'styles') {
            Storage::disk('public_local')->put('common.css', $request->get('json_body')['content']);
        }
        Cache::flush();

        return [
          'message' => 'Настройки успешно обновлены!',
        ];
    }

    public function generateSitemap()
    {
        return Cache::remember('sitemap', now()->addWeek(), function() {
            $sections = [
              'pages' => Page::all(),
              'coins' => Coin::active()->get(),
              'news'  => News::active()->get(),
            ];

            $sitemap = [];

            foreach ($sections as $sectionName => $section) {
                foreach ($section as $page) {
                    switch ($sectionName) {
                        case 'coins':
                            $path = '/moneta/';
                            break;
                        case 'news':
                            $path = Page::find(3)->slug . '/';
                            break;
                        default:
                            $path = '';
                    }
                    $sitemap[] = [
                      'loc'        => config('app.url') . $path . $page->slug,
                      'lastmod'    => $page->updated_at->format('Y-m-d'),
                      'changefreq' => 'daily',
                    ];
                }
            }

            $sitemapContent = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
            $sitemapContent .= '<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">';

            foreach ($sitemap as $item) {
                $sitemapContent .= '<url>';

                foreach ($item as $key => $value) {
                    $sitemapContent .= "<$key>$value</$key>";
                }

                $sitemapContent .= '</url>';
            }

            $sitemapContent .= '</urlset>';

            return response($sitemapContent)->header('Content-Type', 'application/xml');
        });
    }

    public function events()
    {
        return Settings::where('name', 'events')->first();
    }

    public function storeEvent(Request $request)
    {
        throw_if(!$request->ajax(), 404);

        if (!$events = Settings::where('name', 'events')->first()) {
            $events = new Settings([
                'name' => 'events',
                'json_body' => [],
            ]);
        }

        $content = $events->json_body;
        $content[] = [
          'name'  => $request->get('event'),
          'date'  => now()->format('Y-m-d H:i:s')
        ];
        $events->json_body = $content;

        $events->save();

        return [
          'ok'      => true,
          'message' => 'Event успешно засчитан!',
        ];
    }
}
