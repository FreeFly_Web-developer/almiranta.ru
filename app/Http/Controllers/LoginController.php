<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'], ['except' => [
            'show', 'authenticate',
        ]]);
    }

    /**
     * Обработка попыток аутентификации.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function authenticate(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|string',
        ]);

        $data = $request->all();
        $remember = (isset($data['remember_me']) and $data['remember_me']) ? 24000 : 8000;

        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']], $remember))
        {
            return redirect()->intended(url()->previous());
        }

        return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
            'password' => 'Введён неверный логин или пароль.',
        ]);
    }

    /**
     * Logout user and redirect to login page
     *
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        Auth::logout();

        return redirect()->route('login');
    }

    public function show()
    {
        if (Auth::check())
        {
            return redirect()->route('admin.home');
        }

        return view('backend.pages.login');
    }
}
