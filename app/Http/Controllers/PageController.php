<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum'], ['except' => [
            'index',
            'section',
            'getPagesList',
            'getPageById',
            'getPageBySlug',
        ]]);
    }

    public function index(Request $request)
    {
        return (new HomeController())->index();
    }

    public function section($section, $page = null, Request $request)
    {
        $coinsSection = 'moneta';
        $articles = [];
        $parameters = CoinController::COINS_PARAMETERS;

        $breadcrumbs = [
          'home' => [
            'path' => '/',
            'name' => 'home',
            'title' => 'Главная',
          ],
        ];

        if ($section === $coinsSection) {
            $section = new Page();
        } else {
            $section = $this->getPageBySlug($section);
            $breadcrumbs['section'] = [
              'title' => $section->json_body['title'] ?? $section->title,
            ];
        }

        if (!$page) {
            if ($section->id === 3) {
                $request->merge(['isActiveOnly' => true]);
                $articles = (new NewsController())->getNewsList($request);
            }
        } else {
            if ($section->id === 3) {
                $page = (new NewsController())->getNewsArticleBySlug($page);

                $breadcrumbs['section']['path'] = $section->slug;
                $breadcrumbs['page'] = [
                  'title' => $page->name,
                ];
            } else {
                $page = (new CoinController())->getCoinBySlug($page);
                $breadcrumbs['page'] = [
                  'title' => $page->name,
                ];
            }
        }

        return view('pages.page', [
          'page' => $section,
          'subPage' => $page,
          'breadcrumbs' => array_values($breadcrumbs),
          'articles' => $articles,
          'parameters' => $parameters,
        ]);
    }

    public function getPagesList()
    {
        return Cache::remember('pages', now()->addWeek(), function() {
            return Page::where('id', '!=', 6)->get();
        });
    }

    public function getPageById(Page $pageId)
    {
        return $pageId;
    }

    public function getPageBySlug(string $slug)
    {
        return Cache::remember("page-$slug", now()->addWeek(), function() use ($slug) {
            return $this->getPageById(Page::where('slug', "/$slug")->with('meta_tags')->firstOrFail());
        });
    }

    public function updatePage(Page $pageId, Request $request)
    {
        $request->validate([
            'title' => [
              'required',
              'between:2,255',
              Rule::unique('pages')->ignore($pageId->id)
            ],
            'json_body' => 'array',
            'metaTitle'  => 'nullable|between:2,255',
            'metaDescription' => 'nullable|between:2,255',
            'ogTitle'         => 'nullable|between:2,255',
            'ogDescription'   => 'nullable|between:2,255',
            'ogImageWidth'    => 'nullable|number',
            'ogImageHeight'   => 'nullable|number',
        ]);

        foreach ($request->except(['image', 'meta_tags', 'image_object', 'image_alt']) as $name => $value) {
            $pageId->$name = $value;
        }

        if ($pageId->id !== 1) {
          $pageId->slug = '/' . Str::slug($pageId->title);
        }
        (new MetaTagController($pageId, $request->get('meta_tags')))->save();
        $pageId->image_object()->delete();
        $pageId->image_object()->save(new Image(['name' => 'alt', 'value' => $request->get('image_alt')]));

        Cache::flush();
        $pageId->save();

        return [
            'message' => 'Страница успешно обновлена!',
        ];
    }
}
